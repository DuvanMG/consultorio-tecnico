-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Versión del servidor:         10.5.6-MariaDB - mariadb.org binary distribution
-- SO del servidor:              Win64
-- HeidiSQL Versión:             11.0.0.5919
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Volcando estructura de base de datos para consultorio
DROP DATABASE IF EXISTS `consultorio`;
CREATE DATABASE IF NOT EXISTS `consultorio` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `consultorio`;

-- Volcando estructura para tabla consultorio.actividades
DROP TABLE IF EXISTS `actividades`;
CREATE TABLE IF NOT EXISTS `actividades` (
  `Id_actividad` int(11) NOT NULL AUTO_INCREMENT,
  `etapa` int(11) DEFAULT NULL,
  `nombre` varchar(50) DEFAULT NULL,
  `descripcion` varchar(50) DEFAULT NULL,
  `Adjunto` varchar(500) DEFAULT NULL,
  `terminada` int(11) DEFAULT NULL,
  PRIMARY KEY (`Id_actividad`) USING BTREE,
  KEY `etapa` (`etapa`) USING BTREE,
  CONSTRAINT `etapa_actividad` FOREIGN KEY (`etapa`) REFERENCES `etapa` (`Id_etapa`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla consultorio.actividades: ~1 rows (aproximadamente)
DELETE FROM `actividades`;
/*!40000 ALTER TABLE `actividades` DISABLE KEYS */;
INSERT INTO `actividades` (`Id_actividad`, `etapa`, `nombre`, `descripcion`, `Adjunto`, `terminada`) VALUES
	(1, 1, 'Arquitectura', 'Estructura de la finca', 'carta_seminario.docx', 1),
	(6, 4, 'requerimineot ', 'funionales', 'REDUCCION_DE_LAS_DESIGUALDADES.pptx', 1);
/*!40000 ALTER TABLE `actividades` ENABLE KEYS */;

-- Volcando estructura para tabla consultorio.cliente
DROP TABLE IF EXISTS `cliente`;
CREATE TABLE IF NOT EXISTS `cliente` (
  `Id_cliente` int(11) NOT NULL AUTO_INCREMENT,
  `tipo_documento` int(11) DEFAULT NULL,
  `numero_identificacion` varchar(50) DEFAULT NULL,
  `nombre_representante` varchar(50) DEFAULT NULL,
  `apellido_representante` varchar(50) DEFAULT NULL,
  `celular` varchar(50) DEFAULT NULL,
  `direccion` varchar(200) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`Id_cliente`),
  UNIQUE KEY `email` (`email`),
  KEY `tipo_documento` (`tipo_documento`),
  CONSTRAINT `documento_cliente` FOREIGN KEY (`tipo_documento`) REFERENCES `tipo_documento` (`Id_tipo_documento`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla consultorio.cliente: ~4 rows (aproximadamente)
DELETE FROM `cliente`;
/*!40000 ALTER TABLE `cliente` DISABLE KEYS */;
INSERT INTO `cliente` (`Id_cliente`, `tipo_documento`, `numero_identificacion`, `nombre_representante`, `apellido_representante`, `celular`, `direccion`, `email`) VALUES
	(1, 1, '1095954271', 'maria isabel', 'gomez rios ', '3178296876', 'urbanizacion san angel', 'gomezriosm85@gmail.com'),
	(2, 2, '1064107585', 'Darly Andreina', 'Muñoz Gelvis', '3162204249', 'urbanizacion san angel', 'darlyandreinam@gmail.com'),
	(3, 1, '63286625', 'maria teresa', 'rios remolina', '3178239343', 'URBANIZACION SAN ANGEL', 'mariateresa@gmail.com'),
	(4, 1, '', 'Administrador', '', '', '', '');
/*!40000 ALTER TABLE `cliente` ENABLE KEYS */;

-- Volcando estructura para tabla consultorio.cliente_entidad
DROP TABLE IF EXISTS `cliente_entidad`;
CREATE TABLE IF NOT EXISTS `cliente_entidad` (
  `Id_cliente_entidad` int(11) NOT NULL AUTO_INCREMENT,
  `Id_cliente` int(11) DEFAULT NULL,
  `tipo_entidad` int(11) DEFAULT NULL,
  `nombre_empresa` varchar(50) DEFAULT NULL,
  `direccion` varchar(50) DEFAULT NULL,
  `telefono` varchar(50) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`Id_cliente_entidad`) USING BTREE,
  KEY `tipo_entidad` (`tipo_entidad`),
  KEY `Id_cliente` (`Id_cliente`),
  CONSTRAINT `Fk_ClienteEntidad_TipoEntidad` FOREIGN KEY (`tipo_entidad`) REFERENCES `tipo_entidad` (`Id_tipo_entidad`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla consultorio.cliente_entidad: ~4 rows (aproximadamente)
DELETE FROM `cliente_entidad`;
/*!40000 ALTER TABLE `cliente_entidad` DISABLE KEYS */;
INSERT INTO `cliente_entidad` (`Id_cliente_entidad`, `Id_cliente`, `tipo_entidad`, `nombre_empresa`, `direccion`, `telefono`, `email`) VALUES
	(1, 4, 2, 'Udes', NULL, NULL, NULL),
	(3, 1, 3, 'creacionesMARIA', 'calle 32 # 54-09', '6783339', 'creacionesMARIA@gmail.com'),
	(4, 2, 2, 'Organizaciones muñoz', 'calle 32 # 54-09', '6783339', 'OrganizacionesMUÑOZ@gmail.com'),
	(5, 3, 2, 'd409', 'calle 34#65-78', '6435312', 'd409@gmail.com');
/*!40000 ALTER TABLE `cliente_entidad` ENABLE KEYS */;

-- Volcando estructura para tabla consultorio.estado
DROP TABLE IF EXISTS `estado`;
CREATE TABLE IF NOT EXISTS `estado` (
  `Id_estado` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(50) DEFAULT NULL,
  `descripcion` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`Id_estado`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla consultorio.estado: ~2 rows (aproximadamente)
DELETE FROM `estado`;
/*!40000 ALTER TABLE `estado` DISABLE KEYS */;
INSERT INTO `estado` (`Id_estado`, `nombre`, `descripcion`) VALUES
	(1, 'Espera', 'En solicitudes'),
	(2, 'Aprobado', 'realizacion');
/*!40000 ALTER TABLE `estado` ENABLE KEYS */;

-- Volcando estructura para tabla consultorio.etapa
DROP TABLE IF EXISTS `etapa`;
CREATE TABLE IF NOT EXISTS `etapa` (
  `Id_etapa` int(11) NOT NULL AUTO_INCREMENT,
  `proyecto` int(11) NOT NULL,
  `orden` varchar(50) DEFAULT NULL,
  `nombre` varchar(50) DEFAULT NULL,
  `responsable` varchar(50) DEFAULT NULL,
  `fecha_inicio` date DEFAULT NULL,
  `fecha_final` date DEFAULT NULL,
  PRIMARY KEY (`Id_etapa`,`proyecto`) USING BTREE,
  KEY `proyecto` (`proyecto`),
  CONSTRAINT `proyecto_etapa` FOREIGN KEY (`proyecto`) REFERENCES `proyectos` (`Id_proyecto`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla consultorio.etapa: ~2 rows (aproximadamente)
DELETE FROM `etapa`;
/*!40000 ALTER TABLE `etapa` DISABLE KEYS */;
INSERT INTO `etapa` (`Id_etapa`, `proyecto`, `orden`, `nombre`, `responsable`, `fecha_inicio`, `fecha_final`) VALUES
	(1, 1, '1', 'Requerimientos', 'Danilo Andrés', '2021-05-20', '2021-05-27'),
	(4, 3, '2', 'sgss', 'dario', '2021-05-06', '2021-05-13');
/*!40000 ALTER TABLE `etapa` ENABLE KEYS */;

-- Volcando estructura para tabla consultorio.mensajeria
DROP TABLE IF EXISTS `mensajeria`;
CREATE TABLE IF NOT EXISTS `mensajeria` (
  `Id_mensajeria` int(11) NOT NULL AUTO_INCREMENT,
  `proyecto` int(11) DEFAULT NULL,
  `cliente` int(11) DEFAULT NULL,
  `mensaje` varchar(500) DEFAULT NULL,
  `respuesta` varchar(500) DEFAULT NULL,
  `fechaMensaje` datetime DEFAULT NULL,
  `fechaRespuesta` datetime DEFAULT NULL,
  PRIMARY KEY (`Id_mensajeria`),
  KEY `proyecto` (`proyecto`),
  KEY `cliente` (`cliente`),
  CONSTRAINT `cliente_mensajeria` FOREIGN KEY (`cliente`) REFERENCES `cliente` (`Id_cliente`),
  CONSTRAINT `proyecto_mensajeria` FOREIGN KEY (`proyecto`) REFERENCES `proyectos` (`Id_proyecto`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla consultorio.mensajeria: ~6 rows (aproximadamente)
DELETE FROM `mensajeria`;
/*!40000 ALTER TABLE `mensajeria` DISABLE KEYS */;
INSERT INTO `mensajeria` (`Id_mensajeria`, `proyecto`, `cliente`, `mensaje`, `respuesta`, `fechaMensaje`, `fechaRespuesta`) VALUES
	(1, 3, 3, 'ashxbajshbaxjshx', NULL, '2021-05-18 23:54:28', NULL),
	(2, 3, 3, 'Es un sistema que almacena los cambios realizados sobre un archivo o un conjunto de archivos ', NULL, '2021-05-18 23:55:03', NULL),
	(3, 3, 3, NULL, ' que podriamos recuperar versiones especificas del codigo que vayamos desarrollando durante ese tiempo que dure nuestro trabajo', NULL, '2021-05-18 23:56:04'),
	(4, 3, 3, NULL, 'Es un software de VCS local que permite a los desarrolladores guardar instantaneas de sus', NULL, '2021-05-19 00:02:02'),
	(5, 3, 3, NULL, 'knsasfna', NULL, '2021-05-19 21:57:02'),
	(6, 3, 3, 'ñ mñ ,ñ', NULL, '2021-05-20 08:58:02', NULL),
	(7, 3, 3, NULL, 'Es un software de VCS local que permite a los desarrolladores guardar instantaneas de sus', NULL, '2021-05-20 09:06:12');
/*!40000 ALTER TABLE `mensajeria` ENABLE KEYS */;

-- Volcando estructura para tabla consultorio.proyectos
DROP TABLE IF EXISTS `proyectos`;
CREATE TABLE IF NOT EXISTS `proyectos` (
  `Id_proyecto` int(11) NOT NULL AUTO_INCREMENT,
  `solicitud_servicio` int(11) DEFAULT NULL,
  `servicio` int(11) DEFAULT NULL,
  `descripcion` varchar(500) DEFAULT NULL,
  `responsable` varchar(50) DEFAULT NULL,
  `fecha_inicio` date DEFAULT NULL,
  `fecha_final` date DEFAULT NULL,
  `finalizado` int(11) DEFAULT 0,
  PRIMARY KEY (`Id_proyecto`),
  KEY `solicitud_servicio` (`solicitud_servicio`),
  KEY `servicio` (`servicio`),
  CONSTRAINT `servicio_proyecto` FOREIGN KEY (`servicio`) REFERENCES `servicio` (`Id_servicio`),
  CONSTRAINT `solicitudServicio_proyecto` FOREIGN KEY (`solicitud_servicio`) REFERENCES `solicitud_servicio` (`Id_solicitud_servicio`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla consultorio.proyectos: ~6 rows (aproximadamente)
DELETE FROM `proyectos`;
/*!40000 ALTER TABLE `proyectos` DISABLE KEYS */;
INSERT INTO `proyectos` (`Id_proyecto`, `solicitud_servicio`, `servicio`, `descripcion`, `responsable`, `fecha_inicio`, `fecha_final`, `finalizado`) VALUES
	(1, 2, 3, 'creación de la estructura de la finca ...', 'Danilo Andrés', '2021-05-18', '2021-11-23', 1),
	(3, 4, 2, 'necesito los planos para la construcción de planta de producción y lavanderia', 'Darío Ruiz', '2021-05-18', '2021-10-30', 1),
	(4, 3, 5, '\\^v^/', 'Marcela Marin', '2021-05-25', '2021-06-30', 0),
	(5, 1, 4, 'Personal de logística', 'Marcelo Perez', '2021-06-07', '2021-10-29', 0),
	(8, 1, 5, 'En el local de las cámaras y manejo de las aulas virtuales', 'Alfredo Rios', '2021-06-13', '2022-01-08', 0),
	(9, 1, 3, 'Del edificio chibcha en el ultimo piso.', 'Mario Hernandez', '2021-07-01', '2021-12-31', 0);
/*!40000 ALTER TABLE `proyectos` ENABLE KEYS */;

-- Volcando estructura para tabla consultorio.rol
DROP TABLE IF EXISTS `rol`;
CREATE TABLE IF NOT EXISTS `rol` (
  `Id_rol` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(50) DEFAULT NULL,
  `descripcion` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`Id_rol`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla consultorio.rol: ~2 rows (aproximadamente)
DELETE FROM `rol`;
/*!40000 ALTER TABLE `rol` DISABLE KEYS */;
INSERT INTO `rol` (`Id_rol`, `nombre`, `descripcion`) VALUES
	(1, 'Cliente', 'personal'),
	(2, 'Administrador', NULL);
/*!40000 ALTER TABLE `rol` ENABLE KEYS */;

-- Volcando estructura para tabla consultorio.servicio
DROP TABLE IF EXISTS `servicio`;
CREATE TABLE IF NOT EXISTS `servicio` (
  `Id_servicio` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(50) DEFAULT NULL,
  `descripcion` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`Id_servicio`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla consultorio.servicio: ~6 rows (aproximadamente)
DELETE FROM `servicio`;
/*!40000 ALTER TABLE `servicio` DISABLE KEYS */;
INSERT INTO `servicio` (`Id_servicio`, `nombre`, `descripcion`) VALUES
	(1, 'Diseño', 'Diseño De Redes Hidráulicas, Sanitarias, Gas e Incendio'),
	(2, 'Diseño', 'Diseño Arquitectónico'),
	(3, 'Diseño', 'Diseño Estructural'),
	(4, 'Interventoria', 'Subcontratación De InterventorÍa De Obras Civiles'),
	(5, 'Diagnostico', 'Redes tecnológicas'),
	(6, 'ejercicio', 'manejo del cuerpo');
/*!40000 ALTER TABLE `servicio` ENABLE KEYS */;

-- Volcando estructura para tabla consultorio.solicitud_servicio
DROP TABLE IF EXISTS `solicitud_servicio`;
CREATE TABLE IF NOT EXISTS `solicitud_servicio` (
  `Id_solicitud_servicio` int(11) NOT NULL AUTO_INCREMENT,
  `cliente_entidad` int(11) DEFAULT NULL,
  `servicio` int(11) DEFAULT NULL,
  `estado` int(11) DEFAULT NULL,
  `fecha_solicitud` date DEFAULT NULL,
  `descripcion` text DEFAULT NULL,
  PRIMARY KEY (`Id_solicitud_servicio`),
  KEY `servicio` (`servicio`),
  KEY `tipo_entidad` (`cliente_entidad`) USING BTREE,
  KEY `estado` (`estado`),
  CONSTRAINT `clienteEntidad_solicitudServicio` FOREIGN KEY (`cliente_entidad`) REFERENCES `cliente_entidad` (`Id_cliente_entidad`),
  CONSTRAINT `estado_solicitud` FOREIGN KEY (`estado`) REFERENCES `estado` (`Id_estado`),
  CONSTRAINT `servicio_solicitud` FOREIGN KEY (`servicio`) REFERENCES `servicio` (`Id_servicio`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla consultorio.solicitud_servicio: ~4 rows (aproximadamente)
DELETE FROM `solicitud_servicio`;
/*!40000 ALTER TABLE `solicitud_servicio` DISABLE KEYS */;
INSERT INTO `solicitud_servicio` (`Id_solicitud_servicio`, `cliente_entidad`, `servicio`, `estado`, `fecha_solicitud`, `descripcion`) VALUES
	(1, 1, NULL, NULL, NULL, 'Propiedad local administrativa'),
	(2, 3, 3, 1, '2021-05-18', 'creación de la estructura de la finca ...'),
	(3, 4, 5, 1, '2021-05-25', '\\^v^/'),
	(4, 5, 2, 1, '2021-05-18', 'necesito los planos para la construcción de planta de producción y lavanderia');
/*!40000 ALTER TABLE `solicitud_servicio` ENABLE KEYS */;

-- Volcando estructura para tabla consultorio.tipo_documento
DROP TABLE IF EXISTS `tipo_documento`;
CREATE TABLE IF NOT EXISTS `tipo_documento` (
  `Id_tipo_documento` int(11) NOT NULL AUTO_INCREMENT,
  `nombre_tipo` varchar(50) DEFAULT NULL,
  `tipo_persona` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`Id_tipo_documento`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla consultorio.tipo_documento: ~6 rows (aproximadamente)
DELETE FROM `tipo_documento`;
/*!40000 ALTER TABLE `tipo_documento` DISABLE KEYS */;
INSERT INTO `tipo_documento` (`Id_tipo_documento`, `nombre_tipo`, `tipo_persona`) VALUES
	(1, 'Cedula De Ciudadania', 'Natural'),
	(2, 'Tarjeta De Identidad', 'Natural'),
	(3, 'Pasaporte', 'Natural'),
	(4, 'Documento Extranjero', 'Natural'),
	(5, 'Nit', 'Juridico'),
	(6, 'Organización', 'Jurídico');
/*!40000 ALTER TABLE `tipo_documento` ENABLE KEYS */;

-- Volcando estructura para tabla consultorio.tipo_entidad
DROP TABLE IF EXISTS `tipo_entidad`;
CREATE TABLE IF NOT EXISTS `tipo_entidad` (
  `Id_tipo_entidad` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(50) DEFAULT NULL,
  `descripcion` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`Id_tipo_entidad`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla consultorio.tipo_entidad: ~5 rows (aproximadamente)
DELETE FROM `tipo_entidad`;
/*!40000 ALTER TABLE `tipo_entidad` DISABLE KEYS */;
INSERT INTO `tipo_entidad` (`Id_tipo_entidad`, `nombre`, `descripcion`) VALUES
	(1, 'Colegios', NULL),
	(2, 'Empresas Privadas', NULL),
	(3, 'Empresas Publicas', NULL),
	(4, 'Conjuntos', NULL);
/*!40000 ALTER TABLE `tipo_entidad` ENABLE KEYS */;

-- Volcando estructura para procedimiento consultorio.usp_actualizarActividad
DROP PROCEDURE IF EXISTS `usp_actualizarActividad`;
DELIMITER //
CREATE PROCEDURE `usp_actualizarActividad`(
	IN `xactividades` INT(11),
	IN `xdescripcion` VARCHAR(500),
	IN `xterminada` INT
)
BEGIN
		UPDATE actividades SET Adjunto = xdescripcion, terminada = xterminada
		WHERE Id_actividad = xactividades;
	END//
DELIMITER ;

-- Volcando estructura para procedimiento consultorio.usp_actualizarClienteAdmin
DROP PROCEDURE IF EXISTS `usp_actualizarClienteAdmin`;
DELIMITER //
CREATE PROCEDURE `usp_actualizarClienteAdmin`(
	IN `xId_cliente` INT,
	IN `xcelular` VARCHAR(50),
	IN `xdireccion` VARCHAR(50),
	IN `xemail` VARCHAR(200)
)
BEGIN
		UPDATE cliente SET celular = xcelular, direccion = xdireccion, email = xemail
			WHERE Id_cliente = xId_cliente;
	END//
DELIMITER ;

-- Volcando estructura para procedimiento consultorio.usp_actualizarCliente_entidad
DROP PROCEDURE IF EXISTS `usp_actualizarCliente_entidad`;
DELIMITER //
CREATE PROCEDURE `usp_actualizarCliente_entidad`(
	IN `xcliente_entidad` INT(11),
	IN `xtipo_documento` INT(11),
	IN `xidentificacion_cliente` INT(11),
	IN `xtipo_entidad` INT(11),
	IN `xnombre_empresa` VARCHAR(50),
	IN `xdireccion` VARCHAR(50),
	IN `xtelefono` VARCHAR(50),
	IN `xemail` VARCHAR(50)
)
BEGIN
		UPDATE cliente_entidad SET Id_cliente_entidad = xcliente_entidad, tipo_documento = xtipo_documento, identificacion_cliente = xidentificacion_cliente, tipo_entidad = xtipo_entidad, nombre_empresa = xnombre_empresa, direccion = xdireccion, telefono = xtelefono, email = xemail
		WHERE Id_cliente_entidad = xcliente_entidad;
	END//
DELIMITER ;

-- Volcando estructura para procedimiento consultorio.usp_actualizarDatosPersonales
DROP PROCEDURE IF EXISTS `usp_actualizarDatosPersonales`;
DELIMITER //
CREATE PROCEDURE `usp_actualizarDatosPersonales`(
	IN `xId_cliente` INT,
	IN `xcelular` VARCHAR(50),
	IN `xdireccion` VARCHAR(50),
	IN `xemail` VARCHAR(50)
)
BEGIN
		UPDATE cliente SET celular = xcelular, direccion = xdireccion, email = xemail
		WHERE Id_cliente = xId_cliente;
	END//
DELIMITER ;

-- Volcando estructura para procedimiento consultorio.usp_actualizarDocumento
DROP PROCEDURE IF EXISTS `usp_actualizarDocumento`;
DELIMITER //
CREATE PROCEDURE `usp_actualizarDocumento`(
	IN `xId_tipo_documento` INT,
	IN `xnombre_tipo` VARCHAR(50),
	IN `xtipo_persona` VARCHAR(50)
)
BEGIN
		UPDATE tipo_documento SET nombre_tipo = xnombre_tipo, tipo_persona = xtipo_persona
		WHERE Id_tipo_documento = xId_tipo_documento;
	END//
DELIMITER ;

-- Volcando estructura para procedimiento consultorio.usp_actualizarEntidad
DROP PROCEDURE IF EXISTS `usp_actualizarEntidad`;
DELIMITER //
CREATE PROCEDURE `usp_actualizarEntidad`(
	IN `xId_tipo_entidad` INT,
	IN `xnombre` VARCHAR(50),
	IN `xdescripcion` VARCHAR(50)
)
BEGIN
		UPDATE tipo_entidad SET nombre = xnombre, descripcion = xdescripcion
		WHERE Id_tipo_entidad = xId_tipo_entidad;
	END//
DELIMITER ;

-- Volcando estructura para procedimiento consultorio.usp_actualizarEstado
DROP PROCEDURE IF EXISTS `usp_actualizarEstado`;
DELIMITER //
CREATE PROCEDURE `usp_actualizarEstado`(
	IN `xId_estado` INT,
	IN `xnombre` VARCHAR(50),
	IN `xdescripcion` VARCHAR(50)
)
BEGIN
		UPDATE estado SET nombre = xnombre, descripcion = xdescripcion
		WHERE Id_estado = xId_estado;
	END//
DELIMITER ;

-- Volcando estructura para procedimiento consultorio.usp_actualizarEtapa
DROP PROCEDURE IF EXISTS `usp_actualizarEtapa`;
DELIMITER //
CREATE PROCEDURE `usp_actualizarEtapa`(
	IN `xId_etapa` INT (11),
	IN `xorden` VARCHAR (50),
	IN `xnombre` VARCHAR (50),
	IN `xresponsable` VARCHAR (50),
	IN `xfecha_inicio` DATETIME,
	IN `xfecha_final` DATETIME
)
BEGIN
		UPDATE etapa SET orden = xorden, nombre = xnombre, responsable = xresponsable, fecha_inicio = xfecha_inicio, fecha_final = xfecha_final
		WHERE Id_etapa = xId_etapa;
	END//
DELIMITER ;

-- Volcando estructura para procedimiento consultorio.usp_actualizarProyectoFinalizar
DROP PROCEDURE IF EXISTS `usp_actualizarProyectoFinalizar`;
DELIMITER //
CREATE PROCEDURE `usp_actualizarProyectoFinalizar`(
	IN `xproyecto` INT,
	IN `xfinalizado` INT
)
BEGIN
		UPDATE proyectos SET finalizado = xfinalizado
		WHERE Id_proyecto = xproyecto;
END//
DELIMITER ;

-- Volcando estructura para procedimiento consultorio.usp_actualizarRol
DROP PROCEDURE IF EXISTS `usp_actualizarRol`;
DELIMITER //
CREATE PROCEDURE `usp_actualizarRol`(
	IN `xId_rol` INT,
	IN `xnombre` VARCHAR(50),
	IN `xdescripcion` VARCHAR(50)
)
BEGIN
		UPDATE rol SET nombre = xnombre, descripcion = xdescripcion
		WHERE Id_rol = xId_rol;
	END//
DELIMITER ;

-- Volcando estructura para procedimiento consultorio.usp_actualizarRol_usuario
DROP PROCEDURE IF EXISTS `usp_actualizarRol_usuario`;
DELIMITER //
CREATE PROCEDURE `usp_actualizarRol_usuario`(IN xrol_usuario INT (11), IN xusuario INT (11), IN xrol INT (11))
BEGIN
		UPDATE rol_usuario SET usuario = xusuario, rol = xrol
		WHERE Id_rol_usuario = xrol_usuario;
	END//
DELIMITER ;

-- Volcando estructura para procedimiento consultorio.usp_actualizarServicio
DROP PROCEDURE IF EXISTS `usp_actualizarServicio`;
DELIMITER //
CREATE PROCEDURE `usp_actualizarServicio`(
	IN `xId_servicio` INT,
	IN `xnombre` VARCHAR(50),
	IN `xdescripcion` VARCHAR(50)
)
BEGIN
		UPDATE servicio SET nombre = xnombre, descripcion = xdescripcion
		WHERE Id_servicio = xId_servicio;
	END//
DELIMITER ;

-- Volcando estructura para procedimiento consultorio.usp_actualizarSolicitudServicio
DROP PROCEDURE IF EXISTS `usp_actualizarSolicitudServicio`;
DELIMITER //
CREATE PROCEDURE `usp_actualizarSolicitudServicio`(
	IN `xId_solicitud_servicio` INT (11),
	IN `xdescripcion` VARCHAR(500)
)
BEGIN
		UPDATE solicitud_servicio SET descripcion = xdescripcion
		WHERE Id_solicitud_servicio = xId_solicitud_servicio;
	END//
DELIMITER ;

-- Volcando estructura para procedimiento consultorio.usp_actualizarSolicitud_servicio
DROP PROCEDURE IF EXISTS `usp_actualizarSolicitud_servicio`;
DELIMITER //
CREATE PROCEDURE `usp_actualizarSolicitud_servicio`(
	IN `xsolicitud_servicio` INT,
	IN `xestado` VARCHAR (50)
)
BEGIN
		UPDATE solicitud_servicio SET estado = xestado
		WHERE Id_solicitud_servicio = xsolicitud_servicio;
	END//
DELIMITER ;

-- Volcando estructura para procedimiento consultorio.usp_actualizarUsuario
DROP PROCEDURE IF EXISTS `usp_actualizarUsuario`;
DELIMITER //
CREATE PROCEDURE `usp_actualizarUsuario`(
	IN `xId_usuario` INT(11),
	IN `xusuario` VARCHAR(50),
	IN `xpassword` VARCHAR(50)
)
BEGIN
		UPDATE usuario SET usuario = xusuario, password = xpassword
		WHERE Id_usuario = xId_usuario;
	END//
DELIMITER ;

-- Volcando estructura para procedimiento consultorio.usp_consultarActividadArchivoAdminxId
DROP PROCEDURE IF EXISTS `usp_consultarActividadArchivoAdminxId`;
DELIMITER //
CREATE PROCEDURE `usp_consultarActividadArchivoAdminxId`(
	IN `xId_actividad` INT,
	IN `xId_actividad2` INT,
	IN `x@myparam` INT
)
BEGIN
		SELECT * FROM actividades WHERE Id_actividad = xId_actividad;
	END//
DELIMITER ;

-- Volcando estructura para procedimiento consultorio.usp_consultarActividadesAdminxId
DROP PROCEDURE IF EXISTS `usp_consultarActividadesAdminxId`;
DELIMITER //
CREATE PROCEDURE `usp_consultarActividadesAdminxId`(
	IN `xetapa` INT(11),
	IN `xetapa2` INT,
	IN `x@myparam` INT
)
BEGIN
		SELECT Id_actividad, etapa, nombre, descripcion, terminada FROM actividades WHERE etapa = xetapa ;
	END//
DELIMITER ;

-- Volcando estructura para procedimiento consultorio.usp_consultarCliente
DROP PROCEDURE IF EXISTS `usp_consultarCliente`;
DELIMITER //
CREATE PROCEDURE `usp_consultarCliente`()
BEGIN
		SELECT c.*, td.nombre_tipo FROM cliente c
		INNER JOIN tipo_documento td ON td.Id_tipo_documento = c.tipo_documento;
	END//
DELIMITER ;

-- Volcando estructura para procedimiento consultorio.usp_consultarClienteEntidadxId
DROP PROCEDURE IF EXISTS `usp_consultarClienteEntidadxId`;
DELIMITER //
CREATE PROCEDURE `usp_consultarClienteEntidadxId`(
	IN `xnombre_empresa` VARCHAR(50),
	IN `xemail_empresa` VARCHAR(50)
)
BEGIN
	SELECT * FROM cliente_entidad WHERE nombre_empresa = xnombre_empresa AND email = xemail_empresa;
END//
DELIMITER ;

-- Volcando estructura para procedimiento consultorio.usp_consultarCliente_entidad
DROP PROCEDURE IF EXISTS `usp_consultarCliente_entidad`;
DELIMITER //
CREATE PROCEDURE `usp_consultarCliente_entidad`(
	IN `xcliente_entidad` INT(11)
)
BEGIN
		SELECT * FROM cliente_entidad;
	END//
DELIMITER ;

-- Volcando estructura para procedimiento consultorio.usp_consultarCotizacion
DROP PROCEDURE IF EXISTS `usp_consultarCotizacion`;
DELIMITER //
CREATE PROCEDURE `usp_consultarCotizacion`(
	IN `xcotizacion` INT (11)
)
BEGIN
		SELECT * FROM cotizacion WHERE Id_cotizacion = xcotizacion;
	END//
DELIMITER ;

-- Volcando estructura para procedimiento consultorio.usp_consultarDatosPersonales
DROP PROCEDURE IF EXISTS `usp_consultarDatosPersonales`;
DELIMITER //
CREATE PROCEDURE `usp_consultarDatosPersonales`(
	IN `xId_cliente` INT,
	IN `xId_cliente2` INT
)
BEGIN
 		SELECT c.*,t.nombre_tipo FROM cliente c
 		INNER JOIN tipo_documento t ON t.Id_tipo_documento = c.tipo_documento WHERE c.Id_cliente = xId_cliente;
END//
DELIMITER ;

-- Volcando estructura para procedimiento consultorio.usp_consultarEstado_solicitudservicio
DROP PROCEDURE IF EXISTS `usp_consultarEstado_solicitudservicio`;
DELIMITER //
CREATE PROCEDURE `usp_consultarEstado_solicitudservicio`(
	IN `xestado_solicitudservicio` INT (11)
)
BEGIN
		SELECT * FROM estado_solicitudservicio WHERE Id_estado_solicitudServicio = xestado_solicitudservicio;
	END//
DELIMITER ;

-- Volcando estructura para procedimiento consultorio.usp_consultarEtapasAdminxId
DROP PROCEDURE IF EXISTS `usp_consultarEtapasAdminxId`;
DELIMITER //
CREATE PROCEDURE `usp_consultarEtapasAdminxId`(
	IN `xproyecto` INT (11),
	IN `xproyecto2` INT,
	IN `x@myparam` INT
)
BEGIN
		SELECT e.*,cli.nombre_representante FROM etapa e
		INNER JOIn proyectos p ON p.Id_proyecto = e.proyecto
		INNER JOIN solicitud_servicio ss ON ss.Id_solicitud_servicio = p.solicitud_servicio
		INNER JOIN cliente_entidad ce ON ce.Id_cliente_entidad = ss.cliente_entidad
		INNER JOIN cliente cli ON cli.Id_cliente = ce.Id_cliente WHERE e.proyecto = xproyecto AND e.proyecto = xproyecto2;
	END//
DELIMITER ;

-- Volcando estructura para procedimiento consultorio.usp_consultarMensajeriaxId
DROP PROCEDURE IF EXISTS `usp_consultarMensajeriaxId`;
DELIMITER //
CREATE PROCEDURE `usp_consultarMensajeriaxId`(
	IN `xproyecto` INT,
	IN `xproyecto2` INT
)
BEGIN
		SELECT m.*,cli.nombre_representante,p.responsable FROM mensajeria m 
		INNER JOIN cliente cli ON cli.Id_cliente = m.cliente
		INNER JOIN proyectos p ON p.Id_proyecto = m.proyecto WHERE proyecto = xproyecto AND proyecto = xproyecto2;
END//
DELIMITER ;

-- Volcando estructura para procedimiento consultorio.usp_consultarMessagesAdmin
DROP PROCEDURE IF EXISTS `usp_consultarMessagesAdmin`;
DELIMITER //
CREATE PROCEDURE `usp_consultarMessagesAdmin`(
	IN `xcliente` INT,
	IN `xcliente2` INT,
	IN `x@myparam` INT
)
BEGIN
	SELECT m.*,c.nombre_representante,p.descripcion FROM mensajeria m
	INNER JOIN proyectos p ON p.Id_proyecto = m.proyecto
	INNER JOIN cliente c ON c.Id_cliente = m.cliente WHERE cliente = xcliente AND cliente = xcliente2;
END//
DELIMITER ;

-- Volcando estructura para procedimiento consultorio.usp_consultarProyectos
DROP PROCEDURE IF EXISTS `usp_consultarProyectos`;
DELIMITER //
CREATE PROCEDURE `usp_consultarProyectos`()
BEGIN
		SELECT p.*,c.nombre_representante,s.descripcion,s.nombre FROM proyectos p
		INNER JOIN solicitud_servicio ss ON ss.Id_solicitud_servicio = p.solicitud_servicio
		INNER JOIN cliente_entidad ce ON ce.Id_cliente_entidad = ss.cliente_entidad
		INNER JOIN cliente c ON c.Id_cliente = ce.Id_cliente
		INNER JOIN servicio s ON s.Id_servicio = p.servicio;
END//
DELIMITER ;

-- Volcando estructura para procedimiento consultorio.usp_consultarProyectosxID
DROP PROCEDURE IF EXISTS `usp_consultarProyectosxID`;
DELIMITER //
CREATE PROCEDURE `usp_consultarProyectosxID`(
	IN `xId_proyecto` INT,
	IN `xId_proyecto2` INT
)
BEGIN
		SELECT p.*,s.nombre,cli.nombre_representante,s.descripcion FROM proyectos p
		INNER JOIN servicio s ON s.Id_servicio = p.servicio
		INNER JOIN solicitud_servicio ss ON ss.Id_solicitud_servicio = p.solicitud_servicio
		INNER JOIN cliente_entidad ce ON ce.Id_cliente_entidad = ss.cliente_entidad
		INNER JOIN cliente cli ON cli.Id_cliente = ce.Id_cliente WHERE ce.Id_cliente = xId_proyecto AND ce.Id_cliente = xId_proyecto2;
	END//
DELIMITER ;

-- Volcando estructura para procedimiento consultorio.usp_consultarSolicitud_servicio
DROP PROCEDURE IF EXISTS `usp_consultarSolicitud_servicio`;
DELIMITER //
CREATE PROCEDURE `usp_consultarSolicitud_servicio`()
BEGIN
		SELECT ss.*,s.nombre,e.nombre,cli.nombre_representante,te.nombre FROM solicitud_servicio ss
		INNER JOIN cliente_entidad ce ON ce.Id_cliente_entidad = ss.cliente_entidad
		INNER JOIN cliente cli ON cli.Id_cliente = ce.Id_cliente
		INNER JOIN tipo_entidad te ON te.Id_tipo_entidad = ce.tipo_entidad
		INNER JOIN servicio s ON s.Id_servicio = ss.servicio
		INNER JOIN estado e ON e.Id_estado = ss.estado 
		WHERE e.nombre = 'Espera';
	END//
DELIMITER ;

-- Volcando estructura para procedimiento consultorio.usp_consultarSolicitud_servicioxID
DROP PROCEDURE IF EXISTS `usp_consultarSolicitud_servicioxID`;
DELIMITER //
CREATE PROCEDURE `usp_consultarSolicitud_servicioxID`(
	IN `xId_cliente` INT,
	IN `xId_cliente2` INT
)
BEGIN
		SELECT ss.*,s.nombre,e.nombre,cli.nombre_representante FROM solicitud_servicio ss
		INNER JOIN servicio s ON s.Id_servicio = ss.servicio
		INNER JOIN estado e ON e.Id_estado = ss.estado
		INNER JOIN cliente_entidad ce ON ce.Id_cliente_entidad = ss.cliente_entidad
		INNER JOIN cliente cli ON cli.Id_cliente = ce.Id_cliente WHERE ce.Id_cliente = xId_cliente AND ce.Id_cliente = xId_cliente2;
	END//
DELIMITER ;

-- Volcando estructura para procedimiento consultorio.usp_consultarTipo_documento
DROP PROCEDURE IF EXISTS `usp_consultarTipo_documento`;
DELIMITER //
CREATE PROCEDURE `usp_consultarTipo_documento`(
	IN `xId_tipo_documento` INT(11),
	IN `xnombre_tipo` VARCHAR(50),
	IN `xtipo_persona` VARCHAR(50)
)
BEGIN
		SELECT * FROM tipo_documento WHERE tipo_persona = xtipo_persona;
	END//
DELIMITER ;

-- Volcando estructura para procedimiento consultorio.usp_consultarUsuario
DROP PROCEDURE IF EXISTS `usp_consultarUsuario`;
DELIMITER //
CREATE PROCEDURE `usp_consultarUsuario`(
	IN `xusuario` VARCHAR(50),
	IN `xpassword` VARCHAR(50),
	IN `x@myparam` INT
)
BEGIN
		SELECT * FROM usuario WHERE usuario = xusuario AND password = xpassword;
	END//
DELIMITER ;

-- Volcando estructura para procedimiento consultorio.usp_DeleteActividad
DROP PROCEDURE IF EXISTS `usp_DeleteActividad`;
DELIMITER //
CREATE PROCEDURE `usp_DeleteActividad`(
	IN `xId_actividad` INT,
	IN `xId_actividad2` INT
)
BEGIN
	DELETE FROM actividades
	WHERE Id_actividad = xId_actividad AND Id_actividad;
END//
DELIMITER ;

-- Volcando estructura para procedimiento consultorio.usp_deleteDocumento
DROP PROCEDURE IF EXISTS `usp_deleteDocumento`;
DELIMITER //
CREATE PROCEDURE `usp_deleteDocumento`(
	IN `xId_tipo_documento` INT,
	IN `xId_tipo_documento2` INT
)
BEGIN
	DELETE FROM tipo_documento 
	WHERE Id_tipo_documento = xId_tipo_documento AND Id_tipo_documento = xId_tipo_documento2;
END//
DELIMITER ;

-- Volcando estructura para procedimiento consultorio.usp_deleteEntidad
DROP PROCEDURE IF EXISTS `usp_deleteEntidad`;
DELIMITER //
CREATE PROCEDURE `usp_deleteEntidad`(
	IN `xId_tipo_entidad` INT,
	IN `xId_tipo_entidad2` INT
)
BEGIN
	DELETE FROM tipo_entidad
	WHERE Id_tipo_entidad = xId_tipo_entidad AND Id_tipo_entidad = xId_tipo_entidad2;
END//
DELIMITER ;

-- Volcando estructura para procedimiento consultorio.usp_deleteEstado
DROP PROCEDURE IF EXISTS `usp_deleteEstado`;
DELIMITER //
CREATE PROCEDURE `usp_deleteEstado`(
	IN `xId_estado` INT,
	IN `xId_estado2` INT
)
BEGIN
	DELETE FROM estado
	WHERE Id_estado = xId_estado AND Id_estado = xId_estado2;
END//
DELIMITER ;

-- Volcando estructura para procedimiento consultorio.usp_deleteEtapa
DROP PROCEDURE IF EXISTS `usp_deleteEtapa`;
DELIMITER //
CREATE PROCEDURE `usp_deleteEtapa`(
	IN `xId_etapa` INT,
	IN `xId_etapa2` INT
)
BEGIN
	DELETE FROM etapa
	WHERE Id_etapa = xId_etapa AND Id_etapa = xId_etapa2;
END//
DELIMITER ;

-- Volcando estructura para procedimiento consultorio.usp_deleteRol
DROP PROCEDURE IF EXISTS `usp_deleteRol`;
DELIMITER //
CREATE PROCEDURE `usp_deleteRol`(
	IN `xId_rol` INT,
	IN `xId_rol2` INT
)
BEGIN
	DELETE FROM rol
	WHERE Id_rol = xId_rol AND Id_rol = xId_rol2;
END//
DELIMITER ;

-- Volcando estructura para procedimiento consultorio.usp_deleteServicio
DROP PROCEDURE IF EXISTS `usp_deleteServicio`;
DELIMITER //
CREATE PROCEDURE `usp_deleteServicio`(
	IN `xId_servicio` INT,
	IN `xId_servicio2` INT
)
BEGIN
	DELETE FROM servicio
	WHERE Id_servicio = xId_servicio AND Id_servicio = xId_servicio2;
END//
DELIMITER ;

-- Volcando estructura para procedimiento consultorio.usp_deleteSolicitudServicio
DROP PROCEDURE IF EXISTS `usp_deleteSolicitudServicio`;
DELIMITER //
CREATE PROCEDURE `usp_deleteSolicitudServicio`(
	IN `xId_solicitud_servicio` INT,
	IN `xId_solicitud_servicio2` INT
)
BEGIN
	DELETE FROM solicitud_servicio 
	WHERE Id_solicitud_servicio = xId_solicitud_servicio AND Id_solicitud_servicio = xId_solicitud_servicio2;
END//
DELIMITER ;

-- Volcando estructura para procedimiento consultorio.usp_insertarActividad
DROP PROCEDURE IF EXISTS `usp_insertarActividad`;
DELIMITER //
CREATE PROCEDURE `usp_insertarActividad`(
	IN `xetapa` INT,
	IN `xnombre` VARCHAR(50),
	IN `xdescripcion` CHAR(50),
	IN `xterminada` INT
)
BEGIN
		INSERT INTO actividades (etapa, nombre, descripcion, terminada)
			VALUES (xetapa, xnombre, xdescripcion, xterminada);
	END//
DELIMITER ;

-- Volcando estructura para procedimiento consultorio.usp_insertarCliente
DROP PROCEDURE IF EXISTS `usp_insertarCliente`;
DELIMITER //
CREATE PROCEDURE `usp_insertarCliente`(
	IN `xtipo_documento` INT,
	IN `xnumero_identificacion` VARCHAR(50),
	IN `xnombre_representante` VARCHAR(50),
	IN `xapellido_representante` VARCHAR(50),
	IN `xcelular` VARCHAR(50),
	IN `xdireccion` VARCHAR(50),
	IN `xemail` VARCHAR(50)
)
BEGIN
		INSERT INTO cliente(tipo_documento, numero_identificacion, nombre_representante, 
									apellido_representante, celular, direccion, email) 
									VALUES (xtipo_documento, xnumero_identificacion, xnombre_representante, 
									xapellido_representante, xcelular, xdireccion, xemail);
	SELECT LAST_INSERT_ID ();
	END//
DELIMITER ;

-- Volcando estructura para procedimiento consultorio.usp_insertarCliente_entidad
DROP PROCEDURE IF EXISTS `usp_insertarCliente_entidad`;
DELIMITER //
CREATE PROCEDURE `usp_insertarCliente_entidad`(
	IN `xId_cliente` INT,
	IN `xtipo_entidad` INT,
	IN `xnombre_empresa` VARCHAR(50),
	IN `xdireccion` VARCHAR(50),
	IN `xtelefono` VARCHAR(50),
	IN `xemail` VARCHAR(50)
)
BEGIN
		INSERT INTO cliente_entidad (Id_cliente, tipo_entidad, nombre_empresa, direccion, telefono, email)
			VALUES (xId_cliente, xtipo_entidad, xnombre_empresa, xdireccion, xtelefono, xemail);
	SELECT LAST_INSERT_ID ();
	END//
DELIMITER ;

-- Volcando estructura para procedimiento consultorio.usp_insertarCliente_entidadLogeado
DROP PROCEDURE IF EXISTS `usp_insertarCliente_entidadLogeado`;
DELIMITER //
CREATE PROCEDURE `usp_insertarCliente_entidadLogeado`(
	IN `xId_cliente` INT,
	IN `xtipo_entidad` INT,
	IN `xnombre_empresa` VARCHAR(50),
	IN `xdireccion` VARCHAR(50),
	IN `xtelefono` VARCHAR(50),
	IN `xemail` VARCHAR(50),
	OUT `xlastInsert` INT
)
BEGIN
		INSERT INTO cliente_entidad (Id_cliente, tipo_entidad, nombre_empresa, direccion, telefono, email)
			VALUES (xId_cliente, xtipo_entidad, xnombre_empresa, xdireccion, xtelefono, xemail);
      SET @xlastInsert = LAST_INSERT_ID(); 
END//
DELIMITER ;

-- Volcando estructura para procedimiento consultorio.usp_insertarCotizacion
DROP PROCEDURE IF EXISTS `usp_insertarCotizacion`;
DELIMITER //
CREATE PROCEDURE `usp_insertarCotizacion`(
	IN `xcotizacion` INT (11),
	IN `xsolicitud_servicio` INT (11),
	IN `xfecha_cotizacion` DATE,
	IN `xdocumento` INT (11)
)
BEGIN
		INSERT INTO cotizacion (Id_cotizacion, solicitud_servicio, fecha_cotizacion, documento)
			VALUES (xcotizacion, xsolicitud_servicio, xfecha_cotizacion, xdocumento);
	END//
DELIMITER ;

-- Volcando estructura para procedimiento consultorio.usp_insertarEstado
DROP PROCEDURE IF EXISTS `usp_insertarEstado`;
DELIMITER //
CREATE PROCEDURE `usp_insertarEstado`(
	IN `xnombre` VARCHAR (50),
	IN `xdescripcion` VARCHAR(50)
)
BEGIN
		INSERT INTO estado (nombre,descripcion)
			VALUES (xnombre,xdescripcion);
	END//
DELIMITER ;

-- Volcando estructura para procedimiento consultorio.usp_insertarEtapa
DROP PROCEDURE IF EXISTS `usp_insertarEtapa`;
DELIMITER //
CREATE PROCEDURE `usp_insertarEtapa`(
	IN `xproyecto` INT (11),
	IN `xorden` VARCHAR (50),
	IN `xnombre` VARCHAR (50),
	IN `xresponsable` VARCHAR (50),
	IN `xfecha_inicio` DATE,
	IN `xfecha_final` DATE
)
BEGIN
		INSERT INTO etapa (proyecto, orden, nombre, responsable, fecha_inicio, fecha_final)
			VALUES (xproyecto, xorden, xnombre, xresponsable, xfecha_inicio, xfecha_final);
	END//
DELIMITER ;

-- Volcando estructura para procedimiento consultorio.usp_insertarMensajeriaCliente
DROP PROCEDURE IF EXISTS `usp_insertarMensajeriaCliente`;
DELIMITER //
CREATE PROCEDURE `usp_insertarMensajeriaCliente`(
	IN `xproyecto` INT,
	IN `xcliente` INT,
	IN `xmensaje` VARCHAR(500),
	IN `xfechaMensaje` DATETIME
)
BEGIN
		INSERT INTO mensajeria (proyecto,cliente,mensaje,fechaMensaje)
			VALUES (xproyecto,xcliente,xmensaje,xfechaMensaje);
END//
DELIMITER ;

-- Volcando estructura para procedimiento consultorio.usp_insertarMessagesAdmin
DROP PROCEDURE IF EXISTS `usp_insertarMessagesAdmin`;
DELIMITER //
CREATE PROCEDURE `usp_insertarMessagesAdmin`(
	IN `xproyecto` INT,
	IN `xcliente` INT,
	IN `xrespuesta` VARCHAR(500),
	IN `xfechaRespuesta` DATETIME
)
BEGIN
	INSERT INTO mensajeria (proyecto,cliente,respuesta,fechaRespuesta)
		VALUES (xproyecto,xcliente,xrespuesta,xfechaRespuesta);
END//
DELIMITER ;

-- Volcando estructura para procedimiento consultorio.usp_insertarProyectoSolicitud
DROP PROCEDURE IF EXISTS `usp_insertarProyectoSolicitud`;
DELIMITER //
CREATE PROCEDURE `usp_insertarProyectoSolicitud`(
	IN `xsolicitud_servicio` INT,
	IN `xservicio` INT,
	IN `xdescripcion` VARCHAR (500),
	IN `xresponsable` VARCHAR(50),
	IN `xfecha_inicio` DATE,
	IN `xfecha_final` DATE
)
BEGIN
		INSERT INTO proyectos (solicitud_servicio, servicio, descripcion, responsable, fecha_inicio, fecha_final)
			VALUES (xsolicitud_servicio, xservicio, xdescripcion, xresponsable, xfecha_inicio, xfecha_final);
	END//
DELIMITER ;

-- Volcando estructura para procedimiento consultorio.usp_insertarRol
DROP PROCEDURE IF EXISTS `usp_insertarRol`;
DELIMITER //
CREATE PROCEDURE `usp_insertarRol`(
	IN `xnombre` VARCHAR (50),
	IN `xdescripcion` VARCHAR(50)
)
BEGIN
		INSERT INTO rol (nombre,descripcion)
			VALUES (xnombre,xdescripcion);
	END//
DELIMITER ;

-- Volcando estructura para procedimiento consultorio.usp_insertarSolicitudServicio
DROP PROCEDURE IF EXISTS `usp_insertarSolicitudServicio`;
DELIMITER //
CREATE PROCEDURE `usp_insertarSolicitudServicio`(
	IN `xcliente_entidad` INT,
	IN `xservicio` INT,
	IN `xestado` INT,
	IN `xfecha_solicitud` DATE,
	IN `xdescripcion` TEXT
)
BEGIN
		INSERT INTO solicitud_servicio (cliente_entidad, servicio, estado, fecha_solicitud, descripcion)
			VALUES (xcliente_entidad, xservicio, xestado, xfecha_solicitud, xdescripcion);
	END//
DELIMITER ;

-- Volcando estructura para procedimiento consultorio.usp_insertarTipo_documento
DROP PROCEDURE IF EXISTS `usp_insertarTipo_documento`;
DELIMITER //
CREATE PROCEDURE `usp_insertarTipo_documento`(
	IN `xnombre_tipo` VARCHAR (50),
	IN `xtipo_persona` VARCHAR (50)
)
BEGIN
		INSERT INTO tipo_documento (nombre_tipo, tipo_persona)
			VALUES (xnombre_tipo, xtipo_persona);
	END//
DELIMITER ;

-- Volcando estructura para procedimiento consultorio.usp_insertarTipo_entidad
DROP PROCEDURE IF EXISTS `usp_insertarTipo_entidad`;
DELIMITER //
CREATE PROCEDURE `usp_insertarTipo_entidad`(
	IN `xnombre` VARCHAR (50),
	IN `xdescripcion` VARCHAR(50)
)
BEGIN
		INSERT INTO tipo_entidad (nombre, descripcion)
			VALUES (xnombre,xdescripcion);
	END//
DELIMITER ;

-- Volcando estructura para procedimiento consultorio.usp_insertarTipo_Servicio
DROP PROCEDURE IF EXISTS `usp_insertarTipo_Servicio`;
DELIMITER //
CREATE PROCEDURE `usp_insertarTipo_Servicio`(
	IN `xnombre` VARCHAR(50),
	IN `xdescripcion` VARCHAR(50)
)
BEGIN
		INSERT INTO servicio(nombre,descripcion)
		VALUES (xnombre,xdescripcion);
	END//
DELIMITER ;

-- Volcando estructura para procedimiento consultorio.usp_insertarUsuario
DROP PROCEDURE IF EXISTS `usp_insertarUsuario`;
DELIMITER //
CREATE PROCEDURE `usp_insertarUsuario`(
	IN `xrol` INT (11),
	IN `xcliente` INT,
	IN `xusuario` VARCHAR(50),
	IN `xpassword` VARCHAR(50)
)
BEGIN
		INSERT INTO usuario (rol, cliente, usuario, password)
			VALUES (xrol, xcliente, xusuario, xpassword);
	END//
DELIMITER ;

-- Volcando estructura para tabla consultorio.usuario
DROP TABLE IF EXISTS `usuario`;
CREATE TABLE IF NOT EXISTS `usuario` (
  `Id_usuario` int(11) NOT NULL AUTO_INCREMENT,
  `rol` int(11) DEFAULT NULL,
  `cliente` int(11) DEFAULT NULL,
  `usuario` varchar(50) DEFAULT NULL,
  `password` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`Id_usuario`) USING BTREE,
  UNIQUE KEY `usuario` (`usuario`),
  KEY `Id_rol` (`rol`) USING BTREE,
  KEY `cliente` (`cliente`),
  CONSTRAINT `rol_usuario` FOREIGN KEY (`rol`) REFERENCES `rol` (`Id_rol`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla consultorio.usuario: ~3 rows (aproximadamente)
DELETE FROM `usuario`;
/*!40000 ALTER TABLE `usuario` DISABLE KEYS */;
INSERT INTO `usuario` (`Id_usuario`, `rol`, `cliente`, `usuario`, `password`) VALUES
	(1, 2, 4, 'Duvan0816', '0816'),
	(2, 1, 1, 'maria16', '19980916'),
	(3, 1, 2, 'Darly2507', '2005'),
	(4, 1, 3, 'maria02', '0205');
/*!40000 ALTER TABLE `usuario` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
