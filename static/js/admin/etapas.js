$(document).ready(function () {
    
    $(document).on("click", "#btnAgrEtapa", function () {
    
        var xidProyecto = $("#IdProyecto").val();
        var xnombre = $("#NombreEtapa").val();
        var xresponsable = $("#Responsable").val();
        var xorden = $("#Orden").val();
        var xfechaInicial = $("#fechaInicial").val();
        var xfechaFinal = $("#fechaFinal").val();

        $.ajax({
            method: "POST",
            url: "/verEtapas/",
            data: {
                "idProyecto": xidProyecto,
                "NombreEtapa": xnombre,
                "Responsable": xresponsable,
                "Orden": xorden,
                "FechaInicial": xfechaInicial,
                "FechaFinal": xfechaFinal,
            },
            dataType: 'json'
        }).done(function (json) {
            if (json.xmsg === '') {
                $("#myModal").modal('hide');
                window.location.reload();
            } else {
                alert(json.xmsg);
            }

        }).fail(function (jqXHR, textStatus, errorThrown) {
            alert("Error: Registrando etapa" + textStatus + errorThrown);
        });
    });
    
    $(document).on("click", "#btnAgregarEtapa", function () {
        $("#myModal").modal('toggle');
    });
});

$(document).ready(function () {

    $(document).on("click", "#btnEditEtapa", function () {
        var xmyRowId = $("#idEtapaEdit").val();
        var xNombre = $("#NombreEtapaEdit").val();
        var xResponsableEdit = $("#ResponsableEdit").val();
        var xfechaInicialEdit = $("#fechaInicialEdit").val();
        var xOrdenEtapaEdit = $("#OrdenEtapaEdit").val();
        var xfechaFinalEdit = $("#fechaFinalEdit").val();

    $.ajax({
        method: "POST",
        url: "/editarEtapa/",
        data: {
            "idEtapa": xmyRowId,
            "NombreEtapa": xNombre,
            "Responsable": xResponsableEdit,
            "OrdenEtapa": xOrdenEtapaEdit,
            "fechaInicial": xfechaInicialEdit,
            "fechaFinal": xfechaFinalEdit,
        },
        dataType: 'json'
    }).done(function (json) {
        if (json.xmsg === '') {
            $("#myModal").modal('hide');
            window.location.reload();
        } else {
            alert(json.xmsg);
        }
    })
        .fail(function (jqXHR, textStatus, errorThrown) {
            alert("Error: Al Registrando cambios en la etapa");
        });
    });

    $(document).on("click", ".clBtnEtapa", function () {
        var xRenglon = $(this).closest("tr");
        var xmyRowId = xRenglon.find(".ClidEtapa").text();
        var xOrden = xRenglon.find(".Clorden").text();
        var xNombre = xRenglon.find(".Clnombre").text();
        var xResponsable = xRenglon.find(".ClResponsable").text();
        var xfechaInicial = xRenglon.find(".ClfechaInicial").text();
        var xfechaFinal = xRenglon.find(".ClfechaFinal").text();
        $("#idEtapaEdit").val(xmyRowId);
        $("#OrdenEtapaEdit").val(xOrden);
        $("#NombreEtapaEdit").val(xNombre);
        $("#ResponsableEdit").val(xResponsable);
        $("#fechaInicialEdit").val(xfechaInicial);
        $("#fechaFinalEdit").val(xfechaFinal);
        $("#myModalEtapa").modal('toggle');
    });
});

$(document).ready(function () {

    $(document).on("click", "#btnDelEtapa", function () {
        var xmyRowId = $("#idEtapaDel").val();

    $.ajax({
        method: "POST",
        url: "/deleteEtapa/",
        data: {"idEtapaDel": xmyRowId},
        dataType: 'json'
    }).done(function (json) {
        if (json.xmsg === '') {
            window.location.reload();
        } else {
            alert(json.xmsg);
        }
    })
        .fail(function (jqXHR, textStatus, errorThrown) {
            alert("Error: No se pudo eliminar..");
        });
    });

    $(document).on("click", ".clBtnDelEtapa", function () {
        var xRenglon = $(this).closest("tr");
        var xmyRowId = xRenglon.find(".ClidEtapa").text();
        var xOrden = xRenglon.find(".Clorden").text();
        var xNombre = xRenglon.find(".Clnombre").text();
        var xResponsable = xRenglon.find(".ClResponsable").text();
        var xfechaInicial = xRenglon.find(".ClfechaInicial").text();
        var xfechaFinal = xRenglon.find(".ClfechaFinal").text();
        $("#idEtapaDel").val(xmyRowId);
        $("#OrdenEtapaDel").val(xOrden);
        $("#NombreEtapaDel").val(xNombre);
        $("#ResponsableDel").val(xResponsable);
        $("#fechaInicialDel").val(xfechaInicial);
        $("#fechaFinalDel").val(xfechaFinal);
        $("#myModalDelEtapa").modal('toggle');
    });
});

$(document).ready(function() {
    $('#dataTable').DataTable( {
        "language": {
            "url": "///cdn.datatables.net/plug-ins/1.10.24/i18n/Spanish.json"
        }
    } );
});