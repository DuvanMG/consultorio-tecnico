$(document).ready(function () {

    $(document).on("click", "#btnRegProy", function () {
        var xmyRowId = $("#idSolicitud").val();
        var xNombre = $("#Nombre").val();
        var xTipoEntidad = $("#TipoEntidad").val();
        var xIDServicio = $("#IDServicio").val();
        var xDescripcion = $("#Descripcion").val();
        var xFechaInicial = $("#FechaInicial").val();
        var xresponsable = $("#responsable").val();
        var xfechaFinal = $("#fechaFinal").val();

        $.ajax({
            method: "POST",
            url: "/crearproyectosolicitud",
            data: {
                "IdSolicitud": xmyRowId,
                "Nombre": xNombre,
                "TipoEntidad": xTipoEntidad,
                "Servicio": xIDServicio,
                "Descripcion": xDescripcion,
                "fechaInicial": xFechaInicial,
                "responsable": xresponsable,
                "fechaFinal": xfechaFinal
            },
            dataType: 'json'
        }).done(function (json) {
            if (json.xmsg === '') {
                $("#myModal").modal('hide');
                window.location.reload();
                
            } else {
                alert(json.xmsg);
            }
        }).fail(function (jqXHR, textStatus, errorThrown) {
            alert("Error: Registrando Solicitud/Proyecto " + textStatus);
        });
    });

    $(document).on("click", ".clBtnCrearProyecto", function () {
        var xRenglon = $(this).closest("tr");
        var xmyRowId = xRenglon.find(".ClIdSolicitud").text();
        var xNombre = xRenglon.find(".ClNombre").text();
        var xTipoEntidad = xRenglon.find(".ClTipoEntidad").text();
        var xIDServicio = xRenglon.find(".ClServicio").text();
        var xServicio = xRenglon.find(".ClServicio_Nombre").text();
        var xDescripcion = xRenglon.find(".ClDescripcion").text();
        var xFechaInicial = xRenglon.find(".ClFechaInicial").text();
        $("#idSolicitud").val(xmyRowId);
        $("#Nombre").val(xNombre);
        $("#TipoEntidad").val(xTipoEntidad);
        $("#IDServicio").val(xIDServicio);
        $("#Servicio").val(xServicio);
        $("#Descripcion").val(xDescripcion);
        $("#FechaInicial").val(xFechaInicial);
        $("#myModal").modal('toggle');
    });
    $(document).on("click", "#btnCerrarForma", function () {

        $("#myModal").modal('hide')
    });
});

$(document).ready(function () {

    $(document).on("click", ".clBtnVerSolicitud", function () {
        var xRenglon = $(this).closest("tr");
        var xmyRowId = xRenglon.find(".ClIdSolicitud").text();
        var xNombre = xRenglon.find(".ClNombre").text();
        var xTipoEntidad = xRenglon.find(".ClTipoEntidad").text();
        var xIDServicio = xRenglon.find(".ClServicio").text();
        var xServicio = xRenglon.find(".ClServicio_Nombre").text();
        var xDescripcion = xRenglon.find(".ClDescripcion").text();
        var xFechaInicial = xRenglon.find(".ClFechaInicial").text();
        $("#IdSolicitud").val(xmyRowId);
        $("#nombre").val(xNombre);
        $("#tipoEntidad").val(xTipoEntidad);
        $("#iDServicio").val(xIDServicio);
        $("#servicio").val(xServicio);
        $("#descripcion").val(xDescripcion);
        $("#fechaInicial").val(xFechaInicial);
        $("#myModalSolicitudes").modal('toggle');
    });

    $(document).on("click", "#btnCerrarForma", function () {
        
        $("#myModal").modal('hide')
    });
});