$(document).ready(function () {

    $(document).on("click", ".clBtnVisualizarProyecto", function () {
        var xRenglon = $(this).closest("tr");
        var xmyRowId = xRenglon.find(".ClIdProyecto").text();
        var xNombre = xRenglon.find(".ClNombre").text();
        var xServicio = xRenglon.find(".ClServicio").text();
        var xResponsable = xRenglon.find(".ClResponsable").text();
        var xFechaInicial = xRenglon.find(".ClFechaInicial").text();
        var xFechaFinal = xRenglon.find(".ClFechaFinal").text();
        var xDescripcion = xRenglon.find(".ClDescripcion").text();
        $("#idproyecto").val(xmyRowId);
        $("#Nombre").val(xNombre);
        $("#Servicio").val(xServicio);
        $("#Responsable").val(xResponsable);
        $("#FechaInicial").val(xFechaInicial);
        $("#FechaFinal").val(xFechaFinal);
        $("#Descripcion").val(xDescripcion);
        $("#myModal").modal('toggle');
    });
}); 

$(document).ready(function () {

    $(document).on("click", "#btnRegProy", function () {
        var xmyRowId = $("#idproyectoFinalizarProyecto").val();
        $.ajax({
            method: "POST",
            url: "/finalizarProyectos/",
            data: {
                "idproyecto": xmyRowId
            },
            dataType: 'json'
        }).done(function (json) {
            if (json.xmsg === '') {
                $("#myModalFinalizarProyecto").modal('hide');
                window.open("proyectos");

            } else {
                alert(json.xmsg);
            }

        }).fail(function (jqXHR, textStatus, errorThrown) {
            alert("Error: Registrando etapa" + textStatus + errorThrown);
        });
    });

    $(document).on("click", ".clBtnFinalizarProyecto", function () {
        var xRenglon = $(this).closest("tr");
        var xmyRowId = xRenglon.find(".ClIdProyecto").text();
        var xNombre = xRenglon.find(".ClNombre").text();
        var xServicio = xRenglon.find(".ClServicio").text();
        var xResponsable = xRenglon.find(".ClResponsable").text();
        var xFechaInicial = xRenglon.find(".ClFechaInicial").text();
        var xFechaFinal = xRenglon.find(".ClFechaFinal").text();
        var xDescripcion = xRenglon.find(".ClDescripcion").text();
        $("#idproyectoFinalizarProyecto").val(xmyRowId);
        $("#NombreFinalizarProyecto").val(xNombre);
        $("#ServicioFinalizarProyecto").val(xServicio);
        $("#ResponsableFinalizarProyecto").val(xResponsable);
        $("#FechaInicialFinalizarProyecto").val(xFechaInicial);
        $("#FechaFinalFinalizarProyecto").val(xFechaFinal);
        $("#DescripcionFinalizarProyecto").val(xDescripcion);
        $("#myModalFinalizarProyecto").modal('toggle'); 
       
    }); 
});

$(document).ready(function(){
    $('#dataTable').DataTable({
        "language": {
            "url": "///cdn.datatables.net/plug-ins/1.10.24/i18n/Spanish.json"
        }
    });
});