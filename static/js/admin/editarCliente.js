$(document).ready(function () {

    $(document).on("click", "#btnEditCliente", function () {
        var xmyRowId = $("#idcliente").val();
        var xdireccionCliente = $("#direccion").val();
        var xcelular = $("#celular").val();
        var xemailCliente = $("#email").val();

        $.ajax({
            method: "POST",
            url: "/updateCliente",
            data: {
                "idcliente": xmyRowId,
                "direccionCliente": xdireccionCliente,
                "celular": xcelular,
                "emailCliente": xemailCliente,
            },
            dataType: 'json'
        }).done(function (json) {
            if (json.xmsg === '') {
                $("#myModal").modal('hide');
                window.location.reload();
            } else {
                alert(json.xmsg);
            }
        }).fail(function (jqXHR, textStatus, errorThrown) {
            alert("Error: Registrando cambios en la descripción (log)");
        });
    });

    $(document).on("click", ".clBtnEditarCliente", function () {
        var xRenglon = $(this).closest("tr"); 
        var xmyRowId = xRenglon.find(".Clidcliente").text();
        var xNombre = xRenglon.find(".Clnombre").text();
        var xapellido = xRenglon.find(".Clapellido").text();
        var xdocumento = xRenglon.find(".Cldocumento").text();
        var xNdocumento = xRenglon.find(".ClNdocumento").text();
        var xcelular = xRenglon.find(".Clcelular").text();
        var xdireccion = xRenglon.find(".Cldireccion").text();
        var xemail = xRenglon.find(".Clemail").text();
        $("#idcliente").val(xmyRowId);
        $("#Nombre").val(xNombre);
        $("#apellido").val(xapellido);
        $("#documento").val(xdocumento);
        $("#Ndocumento").val(xNdocumento);
        $("#celular").val(xcelular);
        $("#direccion").val(xdireccion);
        $("#email").val(xemail);
        $("#myModalCLiente").modal('toggle');
    }); 
});