$(document).ready(function () {

    $(document).on("click", "#btnAgregarActividad", function () {
        var xidetapa = $("#xIdEtapa").val();
        var xnombre = $("#nombreAgrActividad").val();
        var xDescripcionAgrActividad = $("#DescripcionAgrActividad").val();

        $.ajax({
            method: "POST",
            url: "/agregarActividad/",
            data: {
                "idetapa": xidetapa,
                "Nombre": xnombre,
                "Descripcion": xDescripcionAgrActividad,
            },
            dataType: 'json'
        }).done(function (json) {
            if (json.xmsg === '') {
                $("#Actividades").modal('hide');
                window.location.reload();
            } else {
                alert(json.xmsg);
            }
        }).fail(function (jqXHR, textStatus, errorThrown) {
            alert("Error: Registrando actividdad " + textStatus + errorThrown);
        });
    });
    
    $(document).on("click", ".clBtnVerActividades", function () {
        $("#Actividades").modal('toggle');
        var xRenglon = $(this).closest("tr");
        var xmyRowId = xRenglon.find(".ClidEtapa").text();
        $('#dataTableAct tbody tr').remove();
        $("#xIdEtapa").val(xmyRowId);

        $.ajax({
            url: "/actividades/",
            type: "POST",
            data: {"xmyRowId": xmyRowId},
            success: function(python_data) {
                if (python_data.status) {
                    // [(1, 1, 'Formación', 'Formación', 0), (3, 1, 'Papeleria', 'Papeleria', 0)]
                    messages = python_data.listaactividades;
                    
                    // Create an empty <tr> element and add it to the 1st position of the table:
                    var tbodyRef = document.getElementById('dataTableAct').getElementsByTagName('tbody')[0];

                    for (const element of messages) { 

                        var row = tbodyRef.insertRow(0);           
                    // Insert new cells (<td> elements) at the 1st and 2nd position of the "new" <tr> element:
                        var cell0 = row.insertCell(0);
                        var cell1 = row.insertCell(1);
                        var cell2 = row.insertCell(2);
                        var cell3 = row.insertCell(3);
                        var cell4 = row.insertCell(4);
                        var cell5 = row.insertCell(5);
                    // Add some text to the new cells:
                        cell0.innerHTML = element[0];
                        cell1.innerHTML = element[2]; 
                        cell2.innerHTML = element[3];
                        cell3.innerHTML = "<a class='text-info clBtnSubirArchivo'><svg xmlns='http://www.w3.org/2000/svg' width='25' height='25' fill='currentColor' class='bi bi-upload' viewBox='0 0 16 16'><path d='M.5 9.9a.5.5 0 0 1 .5.5v2.5a1 1 0 0 0 1 1h12a1 1 0 0 0 1-1v-2.5a.5.5 0 0 1 1 0v2.5a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2v-2.5a.5.5 0 0 1 .5-.5z'/><path d='M7.646 1.146a.5.5 0 0 1 .708 0l3 3a.5.5 0 0 1-.708.708L8.5 2.707V11.5a.5.5 0 0 1-1 0V2.707L5.354 4.854a.5.5 0 1 1-.708-.708l3-3z'/></svg></a>"
                        cell4.innerHTML = "<a class='text-info clBtnDescargarArchivo'><svg xmlns='http://www.w3.org/2000/svg' width='25' height='25' fill='currentColor' class='bi bi-download' viewBox='0 0 16 16'><path d='M.5 9.9a.5.5 0 0 1 .5.5v2.5a1 1 0 0 0 1 1h12a1 1 0 0 0 1-1v-2.5a.5.5 0 0 1 1 0v2.5a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2v-2.5a.5.5 0 0 1 .5-.5z'/><path d='M7.646 11.854a.5.5 0 0 0 .708 0l3-3a.5.5 0 0 0-.708-.708L8.5 10.293V1.5a.5.5 0 0 0-1 0v8.793L5.354 8.146a.5.5 0 1 0-.708.708l3 3z'/></svg></a>"
                        cell5.innerHTML = "<a class='text-danger clBtnDeleteEtapa'><svg xmlns='http://www.w3.org/2000/svg' width='25' height='25' fill='currentColor' class='bi bi-trash-fill' viewBox='0 0 16 16'><path d='M2.5 1a1 1 0 0 0-1 1v1a1 1 0 0 0 1 1H3v9a2 2 0 0 0 2 2h6a2 2 0 0 0 2-2V4h.5a1 1 0 0 0 1-1V2a1 1 0 0 0-1-1H10a1 1 0 0 0-1-1H7a1 1 0 0 0-1 1H2.5zm3 4a.5.5 0 0 1 .5.5v7a.5.5 0 0 1-1 0v-7a.5.5 0 0 1 .5-.5zM8 5a.5.5 0 0 1 .5.5v7a.5.5 0 0 1-1 0v-7A.5.5 0 0 1 8 5zm3 .5v7a.5.5 0 0 1-1 0v-7a.5.5 0 0 1 1 0z'/></svg></a>"
                    // Add Clase a la celda
                        $(cell0).addClass('clIdActividad');
                        $(cell1).addClass('clNombreActividad');
                        $(cell0).hide();
                    }
                } 
            }
        })
    });

    $(document).on("click", ".clBtnDescargarArchivo", function () {
        var xRenglon = $(this).closest("tr");
        var xmyRowId = xRenglon.find(".clIdActividad").text();

        $.ajax({
            url: "/actividadDescargarArchivo/",
            type: "POST",
            data: {"IdActividad": xmyRowId},
            success: function(python_data) {
                if (python_data.status) {
                    var xUrl = "http://127.0.0.1:81/consultorio-tecnico/adjunto/"+python_data.nombreArchivo
                    window.open(xUrl, "_blank");                    
                }
            }
        })
    });

    $(document).on("click", ".clBtnSubirArchivo", function () {
        var xRenglon = $(this).closest("tr");
        var xmyRowId = xRenglon.find(".clIdActividad").text();
        $("#IdActividadSubir").val(xmyRowId);
        $("#SubirArchivo").modal('toggle'); 
    });

    $(document).on("click", "#btnSubir", function () {
        var formData = new FormData(document.getElementById("file"));

        $.ajax({
            method: "post",
            url: "/agregarActividadDocumento/",
            data: formData,
            processData: false,
            contentType: false,
            dataType: 'json',
            async: false
        }).done(function (json) {
            if (json.xmsg === '') {
                $("#Actividades").modal('hide');
                window.location.reload();
            } else {
                alert(json.xmsg);
            }
        })
        .fail(function (jqXHR, textStatus, errorThrown) {
            alert("error al Subir Archivo ....");
        });
    });

    // Eliminar actividad
    $(document).ready(function () {

        $(document).on("click", "#btnDelActividad", function () {
            var xmyRowId = $("#IdActividadDel").val();
    
        $.ajax({
            method: "POST",
            url: "/deleteActividad/",
            data: {
                "IdActividad": xmyRowId,
            },
            dataType: 'json'
        }).done(function (json) {
            if (json.xmsg === '') {
                $("#myModalDelActividad").modal('hide');
                window.location.reload();
            } else {
                alert(json.xmsg);
            }
        })
            .fail(function (jqXHR, textStatus, errorThrown) {
                alert("Error: No se pudo eliminar..");
            });
        });
    
        $(document).on("click", ".clBtnDeleteEtapa", function () {
            var xRenglon = $(this).closest("tr");
            var xmyRowId = xRenglon.find(".clIdActividad").text();
            var xNombre = xRenglon.find(".clNombreActividad").text();
            $("#IdActividadDel").val(xmyRowId);
            $("#NombreActividadDel").val(xNombre);
            $("#myModalDelActividad").modal('toggle');
        });
    });
});
