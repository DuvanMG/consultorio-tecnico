$(document).ready(function () {

    $(document).on("click", "#btnRegProy", function () {
        var xmyRowId = $("#idSolicitudCliente").val();
        var xDescripcion = $("#Descripcion").val();

        $.ajax({
            method: "POST",
            url: "../solicitudesCliente",
            data: {
                "IdSolicitud": xmyRowId,
                "Descripcion": xDescripcion,
            },
            dataType: 'json'
        }).done(function (json) {
            if (json.xmsg === '') {
                $("#myModal").modal('hide');
                window.location.reload();
            } else {
                alert(json.xmsg);
            }
        }).fail(function (jqXHR, textStatus, errorThrown) {
            alert("Error: Registrando cambios en la descripción (log)");
        });
    });

    $(document).on("click", ".clBtnEditarSolicitud", function () {
        var xRenglon = $(this).closest("tr");
        var xmyRowId = xRenglon.find(".Clidsolicitud").text();
        var xNombre = xRenglon.find(".Clnombre").text();
        var xservicio = xRenglon.find(".Clservicio").text();
        var xestado = xRenglon.find(".CLestado").text();
        var xfecha = xRenglon.find(".Clfecha").text();
        var xDescripcion = xRenglon.find(".Cldescripcion").text();
        $("#idSolicitudCliente").val(xmyRowId);
        $("#Nombre").val(xNombre);
        $("#servicio").val(xservicio);
        $("#estado").val(xestado);
        $("#fecha").val(xfecha);
        $("#Descripcion").val(xDescripcion);
        $("#myModal").modal('toggle');
    });

    $(document).on("click", "#btnCerrarForma", function () {
        
        $("#myModal").modal('hide')
    });
});

$(document).ready(function () {

    $(document).on("click", "#btnDelete", function () {
        var xmyRowId = $("#idSolicitudDel").val();

        $.ajax({
            method: "POST",
            url: "../deleteSolicitudesCliente",
            data: {
                "IdSolicitud": xmyRowId,
            },
            dataType: 'json'
        }).done(function (json) {
            if (json.xmsg === '') {
                window.location.reload();
            } else {
                alert(json.xmsg);
            }
        }).fail(function (jqXHR, textStatus, errorThrown) {
            alert("Error: Ya esta en proceso de ejecución");
        });
    });

    $(document).on("click", ".clBtnDeleteSolicitud", function () {
        var xRenglon = $(this).closest("tr");
        var xmyRowId = xRenglon.find(".Clidsolicitud").text();
        $("#idSolicitudDel").val(xmyRowId);
        $("#myModales").modal('toggle');
    });

    $(document).on("click", "#btnCerrarForma", function () {
        
        $("#myModales").modal('hide')
    });
});

$(document).ready(function() {
    $('#dataTable').DataTable({
        "language": {
            "url": "///cdn.datatables.net/plug-ins/1.10.24/i18n/Spanish.json"
        }
    });
});