$(document).ready(function () {

    $(document).on("click", ".clBtnProyectoCliente", function () {
        var xRenglon = $(this).closest("tr");
        var xmyRowId = xRenglon.find(".Clidproyecto").text();
        var xNombre = xRenglon.find(".Clnombre").text();
        var xServicio = xRenglon.find(".ClServicio").text();
        var xDescrSer = xRenglon.find(".ClDescrSer").text();
        var xResponsable = xRenglon.find(".ClResponsable").text();
        var xDescripcion = xRenglon.find(".ClDescripcion").text();
        var xFechainicio = xRenglon.find(".ClFechainicio").text();
        var xFechafinal = xRenglon.find(".ClFechafinal").text();
        $("#idproyecto").val(xmyRowId);
        $("#Nombre").val(xNombre);
        $("#Servicio").val(xServicio);
        $("#DescrSer").val(xDescrSer);
        $("#Responsable").val(xResponsable);
        $("#Descripcion").val(xDescripcion);
        $("#Fechainicio").val(xFechainicio);
        $("#Fechafinal").val(xFechafinal);
        $("#myModal").modal('toggle');
    });
});