$(document).ready(function () {
    
    $(document).on("click", ".clBtnVerActividadesCliente", function () {
        $("#Actividad").modal('toggle');
        var xRenglon = $(this).closest("tr");
        var xmyRowId = xRenglon.find(".myRowId").text();
        $('#dataTableActiv tbody tr').remove();
        
        $.ajax({
            url: "/actividades/",
            type: "POST",
            data: {"xmyRowId": xmyRowId},
            success: function(python_data) {
                if (python_data.status) {
                    // [(1, 1, 'Formación', 'Formación', 0), (3, 1, 'Papeleria', 'Papeleria', 0)]
                    messages = python_data.listaactividades;
                    // Create an empty <tr> element and add it to the 1st position of the table:
                    var tbodyRef = document.getElementById('dataTableActiv').getElementsByTagName('tbody')[0];
                    for (const element of messages) { 
                        var row = tbodyRef.insertRow(0);           
                        // Insert new cells (<td> elements) at the 1st and 2nd position of the "new" <tr> element:
                        var cell0 = row.insertCell(0);
                        var cell1 = row.insertCell(1);
                        var cell2 = row.insertCell(2);
                        var cell3 = row.insertCell(3);
                    // Add some text to the new cells:
                        cell0.innerHTML = element[0];
                        cell1.innerHTML = element[2]; 
                        cell2.innerHTML = element[3];
                        cell3.innerHTML = "<a class='text-info clBtnDescargarArchivo'><svg xmlns='http://www.w3.org/2000/svg' width='25' height='25' fill='currentColor' class='bi bi-download' viewBox='0 0 16 16'><path d='M.5 9.9a.5.5 0 0 1 .5.5v2.5a1 1 0 0 0 1 1h12a1 1 0 0 0 1-1v-2.5a.5.5 0 0 1 1 0v2.5a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2v-2.5a.5.5 0 0 1 .5-.5z'/><path d='M7.646 11.854a.5.5 0 0 0 .708 0l3-3a.5.5 0 0 0-.708-.708L8.5 10.293V1.5a.5.5 0 0 0-1 0v8.793L5.354 8.146a.5.5 0 1 0-.708.708l3 3z'/></svg></a>"
                    // ocultar la celda
                        $(cell0).addClass('clIdActividad');
                        $(cell0).hide();
                    }
                } 
            }
        });
    });

    $(document).on("click", ".clBtnDescargarArchivo", function () {
        var xRenglon = $(this).closest("tr");
        var xmyRowId = xRenglon.find(".clIdActividad").text();
        
        $.ajax({
            url: "/actividadDescargarArchivo/",
            type: "POST",
            data: {"IdActividad": xmyRowId},
            success: function(python_data) {
                if (python_data.status) {
                    window.open("http://127.0.0.1:81/consultorio-tecnico/adjunto/"+python_data.nombreArchivo, "_blank");                    
                } 
            }
        })
    });

    $(document).on("click", ".clBtnSubirArchivo", function () {
        var xRenglon = $(this).closest("tr");
        var xmyRowId = xRenglon.find(".clIdActividad").text();
        $("#IdActividadSubir").val(xmyRowId);
        $("#SubirArchivo").modal('toggle'); 
    });
});

$(document).ready(function() {
    $('#dataTables').DataTable({
        "language": {
            "url": "///cdn.datatables.net/plug-ins/1.10.24/i18n/Spanish.json"
        }
    });
});