$(document).on("click", ".clBtnConfiguracion", function () {

    $.ajax({
        method: "GET",
        url: "/configuracion",
        dataType:"JSON",
        success: function(python_data) {
            if (python_data.status) {
                messages = python_data.listaactividades;

                for (const element of messages) { 
                    $('#IdCliente').val(element[0]);
                    $('#IdTipoDocumento').val(element[1]);
                    $('#NumeroDocumento').val(element[2]);
                    $('#NombreCliente').val(element[3]);
                    $('#ApellidoCliente').val(element[4]);
                    $('#Celular').val(element[5]);
                    $('#Direccion').val(element[6]);
                    $('#Email').val(element[7]);
                    $('#TipoDocumento').val(element[8]);
                    $("#myModale").modal('toggle');
                }
            } 
        }
    })
});

$(document).on("click", "#btnConfiguracion", function () {

    var xIdCliente = $("#IdCliente").val();
    var xCelular = $("#Celular").val();
    var xDireccion = $("#Direccion").val();
    var xEmail = $("#Email").val();

    $.ajax({
        method: "POST",
        url: "/configuracion",
        data: {
            "IdCliente": xIdCliente,
            "Celular": xCelular,
            "Direccion": xDireccion,
            "Email": xEmail,
        },
        dataType: 'json'
    }).done(function (json) {
        if (json.xmsg === '') {
            $("#myModal").modal('hide');
            window.location.reload();
        } else {
            alert(json.xmsg);
        }
    }).fail(function (jqXHR, textStatus, errorThrown) {
        alert("Error: No guardo correctamente");
    });
});
