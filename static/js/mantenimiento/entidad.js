$(document).ready(function () {

    $(document).on("click", "#btnEditEntidad", function () {
        var xmyRowId = $("#idEntidad").val();
        var xNombre = $("#NombreEntidad").val();
        var xDescripcion = $("#DescripcionEntidad").val();

    $.ajax({
        method: "POST",
        url: "/editEntidad",
        data: {
            "idEntidad": xmyRowId,
            "Nombre": xNombre,
            "Descripcion": xDescripcion,
        },
        dataType: 'json'
    }).done(function (json) {
        if (json.xmsg === '') {
            $("#myModal").modal('hide');
            window.location.reload();
        } else {
            alert(json.xmsg);
        }
    })
        .fail(function (jqXHR, textStatus, errorThrown) {
            alert("Error: Registrando cambios en la descripción (log)");
        });
    });

    $(document).on("click", ".clBtnEntidad", function () {
        var xRenglon = $(this).closest("tr");
        var xmyRowId = xRenglon.find(".ClidEntidad").text();
        var xNombre = xRenglon.find(".Clnombre").text();
        var xdescripcion = xRenglon.find(".Cldescripcion").text();
        $("#idEntidad").val(xmyRowId);
        $("#NombreEntidad").val(xNombre);
        $("#DescripcionEntidad").val(xdescripcion);
        $("#myModalEntidad").modal('toggle');
    });
});

$(document).ready(function () {

    $(document).on("click", "#btnDelEntidad", function () {
        var xmyRowId = $("#idEntidadDel").val();

    $.ajax({
        method: "POST",
        url: "/deleteEntidad",
        data: {
            "idEntidadDel": xmyRowId,
        },
        dataType: 'json'
    }).done(function (json) {
        if (json.xmsg === '') {
            window.location.reload();
        } else {
            alert(json.xmsg);
        }
    })
        .fail(function (jqXHR, textStatus, errorThrown) {
            alert("Error: No se pudo eliminar..");
        });
    });
    $(document).on("click", ".clBtnDeleteEntidad", function () {
        var xRenglon = $(this).closest("tr");
        var xmyRowId = xRenglon.find(".ClidEntidad").text();
        var xNombre = xRenglon.find(".Clnombre").text();
        var xdescripcion = xRenglon.find(".Cldescripcion").text();
        $("#idEntidadDel").val(xmyRowId);
        $("#NombreDelEntidad").val(xNombre);
        $("#DescripcionDelEntidad").val(xdescripcion);
        $("#myModalDelEntidad").modal('toggle');
    });
});