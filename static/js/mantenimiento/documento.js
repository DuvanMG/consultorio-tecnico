$(document).ready(function () {

    $(document).on("click", "#btnEditDocumento", function () {
        var xmyRowId = $("#idDocumento").val();
        var xNombre = $("#NombreDocumento").val();
        var xDescripcion = $("#DescripcionDocumento").val();

    $.ajax({
        method: "POST",
        url: "/editDocumento",
        data: {
            "idDocumento": xmyRowId,
            "Nombre": xNombre,
            "Descripcion": xDescripcion,
        },
        dataType: 'json'
    }).done(function (json) {
        if (json.xmsg === '') {
            $("#myModal").modal('hide');
            window.location.reload();
        } else {
            alert(json.xmsg);
        }
    })
            .fail(function (jqXHR, textStatus, errorThrown) {
                alert("Error: Registrando cambios en la descripción (log)");
            });
    });

    $(document).on("click", ".clBtnDocumento", function () {
        var xRenglon = $(this).closest("tr");
        var xmyRowId = xRenglon.find(".ClidDocumento").text();
        var xNombre = xRenglon.find(".Clnombre").text();
        var xdescripcion = xRenglon.find(".Cldescripcion").text();
        $("#idDocumento").val(xmyRowId);
        $("#NombreDocumento").val(xNombre);
        $("#DescripcionDocumento").val(xdescripcion);
        $("#myModalDocumento").modal('toggle');
    });
});

$(document).ready(function () {

    $(document).on("click", "#btnDelDocumento", function () {
        var xmyRowId = $("#idDocumentoDel").val();

    $.ajax({
        method: "POST",
        url: "/deleteDocumento",
        data: {
            "idDocumentoDel": xmyRowId,
        },
        dataType: 'json'
    }).done(function (json) {
        if (json.xmsg === '') {
            window.location.reload();
        } else {
            alert(json.xmsg);
        }
    })
        .fail(function (jqXHR, textStatus, errorThrown) {
            alert("Error: Ya esta en proceso de ejecución");
        });
    });
    $(document).on("click", ".clBtnDeleteDocumento", function () {
        var xRenglon = $(this).closest("tr");
        var xmyRowId = xRenglon.find(".ClidDocumento").text();
        var xNombre = xRenglon.find(".Clnombre").text();
        var xdescripcion = xRenglon.find(".Cldescripcion").text();
        $("#idDocumentoDel").val(xmyRowId);
        $("#NombreDelDocumento").val(xNombre);
        $("#DescripcionDelDocumento").val(xdescripcion);
        $("#myModalDelDocumento").modal('toggle');
    });
});