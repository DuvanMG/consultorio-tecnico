$(document).ready(function () {

    $(document).on("click", "#btnEditEstado", function () {
        var xmyRowId = $("#idEstado").val();
        var xNombre = $("#NombreEstado").val();
        var xDescripcion = $("#DescripcionEstado").val();

    $.ajax({
        method: "POST",
        url: "/editEstado",
        data: {
            "idEstado": xmyRowId,
            "Nombre": xNombre,
            "Descripcion": xDescripcion,
        },
        dataType: 'json'
    }).done(function (json) {
        if (json.xmsg === '') {
            $("#myModal").modal('hide');
            window.location.reload();
        } else {
            alert(json.xmsg);
        }
    })
        .fail(function (jqXHR, textStatus, errorThrown) {
            alert("Error: Registrando cambios en la descripción (log)");
        });
    });

    $(document).on("click", ".clBtnEstado", function () {
        var xRenglon = $(this).closest("tr");
        var xmyRowId = xRenglon.find(".ClidEstado").text();
        var xNombre = xRenglon.find(".Clnombre").text();
        var xdescripcion = xRenglon.find(".Cldescripcion").text();
        $("#idEstado").val(xmyRowId);
        $("#NombreEstado").val(xNombre);
        $("#DescripcionEstado").val(xdescripcion);
        $("#myModalEstado").modal('toggle');
    });
});

$(document).ready(function () {

    $(document).on("click", "#btnDelEstado", function () {
        var xmyRowId = $("#idEstadoDel").val();

    $.ajax({
        method: "POST",
        url: "/deleteEstado",
        data: {
            "idEstadoDel": xmyRowId,
        },
        dataType: 'json'
    }).done(function (json) {
        if (json.xmsg === '') {
            window.location.reload();
        } else {
            alert(json.xmsg);
        }
    })
        .fail(function (jqXHR, textStatus, errorThrown) {
            alert("Error: No se puede eliminar..");
        });
    });
    $(document).on("click", ".clBtnDeleteEstado", function () {
        var xRenglon = $(this).closest("tr");
        var xmyRowId = xRenglon.find(".ClidEstado").text();
        var xNombre = xRenglon.find(".Clnombre").text();
        var xdescripcion = xRenglon.find(".Cldescripcion").text();
        $("#idEstadoDel").val(xmyRowId);
        $("#NombreDelEstado").val(xNombre);
        $("#DescripcionDelEstado").val(xdescripcion);
        $("#myModalDelEstado").modal('toggle');
    });
});