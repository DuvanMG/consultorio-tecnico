$(document).ready(function () {

    $(document).on("click", "#btnEditServicio", function () {
        var xmyRowId = $("#idServicio").val();
        var xNombre = $("#NombreServicio").val();
        var xDescripcion = $("#DescripcionServicio").val();

    $.ajax({
        method: "POST",
        url: "/editServicio",
        data: {
            "idServicio": xmyRowId,
            "Nombre": xNombre,
            "Descripcion": xDescripcion,
        },
        dataType: 'json'
    }).done(function (json) {
        if (json.xmsg === '') {
            $("#myModal").modal('hide');
            window.location.reload();
        } else {
            alert(json.xmsg);
        }
    })
        .fail(function (jqXHR, textStatus, errorThrown) {
            alert("Error: Registrando cambios en la descripción (log)");
        });
    });

    $(document).on("click", ".clBtnServicio", function () {
        var xRenglon = $(this).closest("tr");
        var xmyRowId = xRenglon.find(".ClidServicio").text();
        var xNombre = xRenglon.find(".Clnombre").text();
        var xdescripcion = xRenglon.find(".Cldescripcion").text();
        $("#idServicio").val(xmyRowId);
        $("#NombreServicio").val(xNombre);
        $("#DescripcionServicio").val(xdescripcion);
        $("#myModalServicio").modal('toggle');
    });
});

$(document).ready(function () {

    $(document).on("click", "#btnDelServicio", function () {
        var xmyRowId = $("#idServicioDel").val();

    $.ajax({
        method: "POST",
        url: "/deleteServicio",
        data: {
            "idServicioDel": xmyRowId,
        },
        dataType: 'json'
    }).done(function (json) {
        if (json.xmsg === '') {
            window.location.reload();
        } else {
            alert(json.xmsg);
        }
    })
        .fail(function (jqXHR, textStatus, errorThrown) {
            alert("Error: No se puede eliminar..");
        });
    });
    $(document).on("click", ".clBtnDeleteServicio", function () {
        var xRenglon = $(this).closest("tr");
        var xmyRowId = xRenglon.find(".ClidServicio").text();
        var xNombre = xRenglon.find(".Clnombre").text();
        var xdescripcion = xRenglon.find(".Cldescripcion").text();
        $("#idServicioDel").val(xmyRowId);
        $("#NombreDelServicio").val(xNombre);
        $("#DescripcionDelServicio").val(xdescripcion);
        $("#myModalDelServicio").modal('toggle');
    });
});