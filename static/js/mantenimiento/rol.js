$(document).ready(function () {

    $(document).on("click", "#btnEditRol", function () {
        var xmyRowId = $("#idRol").val();
        var xNombre = $("#NombreRol").val();
        var xDescripcion = $("#DescripcionRol").val();

    $.ajax({
        method: "POST",
        url: "/editRol",
        data: {
            "idRol": xmyRowId,
            "Nombre": xNombre,
            "Descripcion": xDescripcion,
        },
        dataType: 'json'
    }).done(function (json) {
        if (json.xmsg === '') {
            $("#myModal").modal('hide');
            window.location.reload();
        } else {
            alert(json.xmsg);
        }
    })
        .fail(function (jqXHR, textStatus, errorThrown) {
            alert("Error: Registrando cambios en la descripción (log)");
        });
    });

    $(document).on("click", ".clBtnRol", function () {
        var xRenglon = $(this).closest("tr");
        var xmyRowId = xRenglon.find(".ClidRol").text();
        var xNombre = xRenglon.find(".Clnombre").text();
        var xdescripcion = xRenglon.find(".Cldescripcion").text();
        $("#idRol").val(xmyRowId);
        $("#NombreRol").val(xNombre);
        $("#DescripcionRol").val(xdescripcion);
        $("#myModalRol").modal('toggle');
    });
});

$(document).ready(function () {

    $(document).on("click", "#btnDelRol", function () {
        var xmyRowId = $("#idRolDel").val();

    $.ajax({
        method: "POST",
        url: "/deleteRol",
        data: {
            "idRolDel": xmyRowId,
        },
        dataType: 'json'
    }).done(function (json) {
        if (json.xmsg === '') {
            window.location.reload();
        } else {
            alert(json.xmsg);
        }
    })
        .fail(function (jqXHR, textStatus, errorThrown) {
            alert("Error: No se puede eliminar..");
        });
    });
    $(document).on("click", ".clBtnDeleteRol", function () {
        var xRenglon = $(this).closest("tr");
        var xmyRowId = xRenglon.find(".ClidRol").text();
        var xNombre = xRenglon.find(".Clnombre").text();
        var xdescripcion = xRenglon.find(".Cldescripcion").text();
        $("#idRolDel").val(xmyRowId);
        $("#NombreDelRol").val(xNombre);
        $("#DescripcionDelRol").val(xdescripcion);
        $("#myModalDelRol").modal('toggle');
    });
});