import mariadb
import sys

try:
    conn = mariadb.connect(
        host="127.0.0.1",
        port=3306,
        database="consultorio",
        user="root",
        password="123"
        )

except mariadb.Error as e:
    print(f"Error connecting to MariaDB platform: {e}")
    sys.exit(1)

cursor = conn.cursor() 