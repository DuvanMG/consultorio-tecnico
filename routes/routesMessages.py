from flask import Flask, render_template, request, json, redirect, session, jsonify
from appconfig import app
from models.mensajeria import Mensajeria
from datetime import datetime
from models.etapasAdmin import EtapasAdmin
from models.registrarse import RegistroCliente

@app.route("/chat_client/<id_proyecto>/")
def chat_client(id_proyecto):

    if 'IdUsuario' in session and session['Rol'] == 1:
        
        etapas = EtapasAdmin()
        etapas.IdProyecto = id_proyecto
        Etapas = etapas.ObtenerEtapasAdminxId()

        return render_template("cliente/chat_client.html",Id_proyecto = id_proyecto, etapas = Etapas) 

    else:
        return redirect('/')

@app.route("/message/", methods=["GET", "POST"])
def message():

    if request.method == "POST":

        data = request.get_json(force=True, silent=True)
        if data:
            
            idCliente = session['IdCliente']
            Id_proyecto = data["id_proyecto"]
            Message = data["Message"]
            now = datetime.now()
            fecha = now.strftime("%Y/%m/%d %H:%M:%S")
            print(idCliente)
            message = Mensajeria()
            message.IdCliente = idCliente
            message.IdProyecto = Id_proyecto
            message.Mensaje = Message
            message.FechaMensaje = fecha
            message.CrearMensajeria()

            response = {"status": "success","messages": Message}
            return jsonify(response)

        response = {"status": "error"}
        return jsonify(response)

@app.route("/message/<Id_proyecto>/", methods=["GET", "POST"])
def message_get(Id_proyecto):

    if request.method == "GET":

        message = Mensajeria()
        message.IdProyecto = Id_proyecto
        messages_saved = message.ObtenerMensajeriaxId()

        response = {"status": "success","messages_saved": messages_saved}
        return jsonify(response)
   
@app.route("/chat_admin", methods=["GET"])
def chat_admin():     

    if request.method == "GET":

        return render_template("admin/chat_admin.html")

@app.route("/clientes_admin/", methods=["GET","POST"])
def clientes_admin():     

    if request.method == "GET":

        dato = RegistroCliente()
        messages_saved = dato.ObtenerClientes()

        response = {"status": "success","messages_saved": messages_saved}
        return jsonify(response)

@app.route("/clientes_admin_post/", methods=["GET","POST"])
def clientes_admin_post():   

    if request.method == "POST":

        idCliente = request.form['idCliente']

        message = Mensajeria()
        message.IdCliente = idCliente
        messages = message.ObtenerMessagesAdmin()

        if messages:
            for idcli in messages:
                session['idproyecto'] = idcli[1]
                session['idcliente'] = idcli[2]
                break

        response = {"status": "success","messages": messages}
        return jsonify(response)


@app.route("/message_admin/", methods=["GET", "POST"])
def message_admin(): 

    if request.method == "POST":

        data = request.get_json(force=True, silent=True)
        if data:

            idCliente = session['idcliente']
            Id_proyecto = session["idproyecto"]
            Message = data["Message"]
            now = datetime.now()
            fecha = now.strftime("%Y/%m/%d %H:%M:%S")

            message = Mensajeria()
            message.IdCliente = idCliente
            message.IdProyecto = Id_proyecto
            message.Respuesta = Message
            message.FechaRespuesta = fecha
            message.CrearMessagesAdmin()

            response = {"status": "success","messages": Message}
            return jsonify(response)

        response = {"status": "error"}
        return jsonify(response)