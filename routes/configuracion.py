from flask import Flask, render_template, request, json, redirect, session, jsonify, make_response
from appconfig import app
from models.configuracion import DatosPersonales

@app.route('/configuracion', methods=['GET','POST'])
def configuracion():
        
    if request.method == 'GET':

        Usuario = session['UsuarioSis']
        idCliente = session['IdCliente']
        Usuario = Usuario.title()

        configuracion = DatosPersonales()
        configuracion.IdCliente = idCliente
        Configuracion = configuracion.ObtenerDatosPersonales()

        return {"status": True, "xmsg":"", "listaactividades": Configuracion}

    if request.method == 'POST':

        idCliente = request.form['IdCliente']
        celular = request.form['Celular']
        direccion = request.form['Direccion']
        email = request.form['Email']

        solicitud = DatosPersonales()
        solicitud.IdCliente = idCliente
        solicitud.Celular = celular
        solicitud.Direccion = direccion
        solicitud.Email = email
        solicitud.UpdateDatosPersonales()

        responseBody = { "xmsg": "" }
        return make_response(jsonify(responseBody), 200)


