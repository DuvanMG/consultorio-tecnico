from flask import Flask, render_template, request, json, redirect, session, jsonify, make_response
from appconfig import app
from models.tipoRol import TipoRol

@app.route('/rol', methods=['POST','GET'])
def rol():

    if request.method == 'GET':
        
        rol = TipoRol()
        Rol = rol.ObtenerRoles()

        return render_template('admin/table_rol.html', Roles = Rol)

    if request.method == 'POST':

        Nombre = request.form['Nombre']
        Descripcion = request.form['Descripcion']

        rol = TipoRol()
        rol.Nombre = Nombre
        rol.Descripcion = Descripcion
        rol.CrearRol()

        return redirect('rol')

    return render_template('admin/table_rol.html')

@app.route('/editRol', methods=['POST'])
def editRol():

    if request.method == 'POST':

        idRol = request.form['idRol']
        Nombre = request.form['Nombre']
        Descripcion = request.form['Descripcion']

        documento = TipoRol()
        documento.IdRol = idRol
        documento.Nombre = Nombre
        documento.Descripcion = Descripcion
        documento.EditarRol()

        responseBody = { "xmsg": "" }
        return make_response(jsonify(responseBody), 200)
    
@app.route('/deleteRol', methods=['POST'])
def deleteRol():

    if request.method == 'POST':

        idRolDel = request.form['idRolDel']

        solicitud = TipoRol()
        solicitud.IdRol = idRolDel
        solicitud.DeleteRol()

        responseBody = { "xmsg": "" }
        return make_response(jsonify(responseBody), 200)