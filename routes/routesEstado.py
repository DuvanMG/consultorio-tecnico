from flask import Flask, render_template, request, json, redirect, session, jsonify, make_response
from appconfig import app
from models.estado import Estados

@app.route('/estado', methods=['POST','GET'])
def estado():

    if request.method == 'GET':
        
        estado = Estados()
        Estado = estado.ObtenerEstado()

        return render_template('admin/table_estado.html', Estados = Estado)

    if request.method == 'POST':

        Nombre = request.form['Nombre']
        Descripcion = request.form['Descripcion']

        rol = Estados()
        rol.Nombre = Nombre
        rol.Descripcion = Descripcion
        rol.CrearEstado()

        return redirect('/estado')

    return render_template('admin/table_estado.html')

@app.route('/editEstado', methods=['POST'])
def editEstado():

    if request.method == 'POST':

        idEstado = request.form['idEstado']
        nombre = request.form['Nombre']
        descripcion = request.form['Descripcion']

        documento = Estados()
        documento.IdEstado = idEstado
        documento.Nombre = nombre
        documento.Descripcion = descripcion
        documento.EditarEstado()

        responseBody = { "xmsg": "" }
        return make_response(jsonify(responseBody), 200)
    
    return render_template('admin/table_documento.html')

@app.route('/deleteEstado', methods=['POST'])
def deleteEstado():

    if request.method == 'POST':

        idEstadoDel = request.form['idEstadoDel']

        solicitud = Estados()
        solicitud.IdEstado = idEstadoDel
        solicitud.DeleteEstado()

        responseBody = { "xmsg": "" }
        return make_response(jsonify(responseBody), 200)
