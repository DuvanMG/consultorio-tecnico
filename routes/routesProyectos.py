from flask import Flask, render_template, request, json, redirect, session, jsonify, make_response
from models.proyectos import Proyectos
from models.gestionClientes import GestionClientes
from appconfig import app

@app.route('/proyectosCliente')
def proyectosCliente():

   if 'IdUsuario' in session and session['Rol'] == 1:
        
      Usuario = session['UsuarioSis']
      IdCliente = session['IdCliente']

      proyectos = GestionClientes()
      proyectos.IdCliente = IdCliente
      Proyectos = proyectos.ObtenerProyectosxId()
 
      return render_template('cliente/proyectosCliente.html', Proyectos = Proyectos, usuario = Usuario) 

   else:
      return redirect('/')
      
@app.route('/proyectos', methods=['GET'])
def ObtenerProyectos():

   proyecto = Proyectos()
   proyectos = proyecto.ObtenerProyectos()

   return render_template('admin/table_proyectos.html', Proyectos = proyectos)

@app.route('/finalizarProyectos/', methods=['POST'])
def finalizarProyectos():

   if request.method == 'POST':

      try:
         idProyecto = request.form['idproyecto']

         documentos = Proyectos()
         documentos.IdProyecto = idProyecto
         documentos.finalizarProyectoAdmin()

      except KeyError:
         pass
      
      responseBody = { "xmsg": "" }
      return make_response(jsonify(responseBody), 200)



