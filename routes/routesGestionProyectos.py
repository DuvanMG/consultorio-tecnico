from flask import Flask, render_template, request, json, redirect, session
from models.servicio import Servicio
from models.tipoEntidad import TipoEntidad
from models.solicitud import Solicitud
from models.proyectos import Proyectos
from appconfig import app
from flask import jsonify, make_response

@app.route('/crearproyectos', methods=['GET'])
def ObtenerServicioandTipoentidad():

    servicio = Servicio()
    servicios = servicio.ObtenerServicio()

    entidad = TipoEntidad()
    entidades = entidad.ObtenerTipoEntidad()
    
    return render_template('admin/crear_proyecto.html', servicio = servicios, entidad = entidades)

@app.route('/crearproyectos', methods=['GET', 'POST'])
def crearproyectos():

    if request.method == 'POST': 

        responsable = request.form['responsable']
        servicio = request.form['servicio']
        fechaInicial = request.form['fechaInicial']
        fechaFinal = request.form['fechaFinal']
        descripcion = request.form['descripcion']

        registro = Proyectos()
        registro.Responsable = responsable
        registro.IdServicio = servicio
        registro.FechaInicial = fechaInicial
        registro.FechaFinal = fechaFinal
        registro.Descripcion = descripcion
        registro.CrearProyectoSolicitud()

        return redirect('/proyectos')

    return render_template('admin/crear_proyecto.html')


@app.route('/proyectosolicitud', methods=['GET'])
def ObtenerSolicitudes():

    dato = Solicitud()
    datos = dato.ObtenerSolicitudes()

    return render_template('admin/crear_solicitudes.html', documentos = datos)


@app.route('/crearproyectosolicitud', methods=['GET', 'POST'])
def CrearProyectoSolicitud():

    if request.method == 'POST':
        
        try:
            idSolicitud = request.form['IdSolicitud']
            servicio = request.form['Servicio']
            descripcion = request.form['Descripcion']
            responsable = request.form['responsable']
            fechaInicial = request.form['fechaInicial']
            fechaFinal = request.form['fechaFinal']     

            registro = Proyectos()
            registro.IdSolicitud = idSolicitud
            registro.IdServicio = servicio
            registro.Descripcion = descripcion
            registro.Responsable = responsable
            registro.FechaInicial = fechaInicial
            registro.FechaFinal = fechaFinal
            registro.CrearProyectoSolicitud()

        except KeyError :
            pass

        responseBody = { "xmsg": "" }
        return make_response(jsonify(responseBody), 200)