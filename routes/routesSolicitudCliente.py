from flask import Flask, render_template, request, json, redirect, session
from appconfig import app
from models.solicitud import Solicitud
from models.servicio import Servicio
from models.tipoEntidad import TipoEntidad
from models.registroClienteEntidad import RegistroClienteEntidad

@app.route('/solicitud', methods=['GET'])
def solicitud():

   if 'IdUsuario' in session and session['Rol'] == 1:
        
      Usuario = session['UsuarioSis']

      servicio = Servicio()
      servicios = servicio.ObtenerServicio()

      entidad = TipoEntidad()
      entidades = entidad.ObtenerTipoEntidad()

      return render_template('cliente/solicitud.html', usuario = Usuario, servicio = servicios, entidad = entidades) 

   else:
      return redirect('/')


   
@app.route('/solicitud', methods=['POST','GET'])
def SolicitudCliente():

   if request.method == 'POST': 

         # registrar la entidad
      idCliente = session['IdCliente']
      nombreEmpresa = request.form['nombreEmpresa']
      tipoEntidad = request.form['tipoEntidad']
      direccionEmpresa = request.form['direccionEmpresa']
      telefonoEmpresa = request.form['telefonoEmpresa']
      emailEmpresa = request.form['emailEmpresa']

      registro = RegistroClienteEntidad()
      registro.IdCliente = idCliente
      registro.NombreEmpresa = nombreEmpresa
      registro.TipoEntidad = tipoEntidad
      registro.DireccionEmpresa = direccionEmpresa
      registro.TelefonoEmpresa = telefonoEmpresa
      registro.EmailEmpresa = emailEmpresa
      obtenerIdClienteEntidad = registro.CrearClienteEntidadLogeado()

      if obtenerIdClienteEntidad:
         for Id in obtenerIdClienteEntidad:
            session['IdClienteEntidad'] = Id

         # registrar la solicitud
      idClienteEntidad = session['IdClienteEntidad']
      servicio = request.form['servicio']
      fechaSolicitud = request.form['fechaSolicitud']
      descripcion = request.form['descripcion']

      solicitud = Solicitud()
      solicitud.IdClienteEntidad = idClienteEntidad
      solicitud.Servicio = servicio
      solicitud.FechaSolicitud = fechaSolicitud
      solicitud.Descripcion = descripcion
      solicitud.CrearSolicitudAdmin()

      return redirect('/solicitudesCliente')

