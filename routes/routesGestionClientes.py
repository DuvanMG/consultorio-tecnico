from flask import Flask, render_template, request, json, redirect, session, make_response, jsonify
from models.registrarse import RegistroCliente
from models.tipoDocumento import TipoDocumento
from models.gestionClientes import GestionClientes
from models.servicio import Servicio
from models.tipoEntidad import TipoEntidad
from models.solicitud import Solicitud
from models.registroClienteEntidad import RegistroClienteEntidad
from models.gestionClienteAdmin import GestionClienteAdmin
from appconfig import app

@app.route('/administrarClientes')
def ObteneradministrarClientes():

    if 'IdUsuario' in session and session['Rol'] == 2:
        
        documentos = TipoDocumento()
        Documentos = documentos.ObtenerTipoDocumento()

        dato = RegistroCliente()
        datos = dato.ObtenerClientes()

        return render_template('admin/administrar_cliente.html', documentos = Documentos, Datos = datos) 

    else:
        return redirect('/')
        
@app.route('/updateCliente', methods=['GET', 'POST'])
def updateCliente():

    if request.method == 'POST':

        idcliente = request.form['idcliente']
        direccionCliente = request.form['direccionCliente']
        celular = request.form['celular']
        emailCliente = request.form['emailCliente']

        update = GestionClienteAdmin()
        update.IdCliente = idcliente
        update.DireccionCliente = direccionCliente
        update.Celular = celular
        update.EmailCliente = emailCliente
        update.updateClienteAdmin()

        responseBody = { "xmsg": "" }
        return make_response(jsonify(responseBody), 200)
   
@app.route('/administrarClientes', methods=['GET', 'POST'])
def administrarClientes():

    if request.method == 'POST':

        # Registro del cliente
        nombreCliente = request.form['nombreCliente']
        apellidoCliente = request.form['apellidoCliente']
        tipodocumento = request.form['tipodocumento']
        numerodocumento = request.form['numerodocumento']
        direccionCliente = request.form['direccionCliente']
        celular = request.form['celular']
        emailCliente = request.form['emailCliente']        

        registro = RegistroCliente()
        registro.TipoDocumento = tipodocumento
        registro.numeroDocumento = numerodocumento
        registro.NombreCliente = nombreCliente
        registro.ApellidoCliente = apellidoCliente
        registro.Celular = celular
        registro.DireccionCliente = direccionCliente
        registro.EmailCliente = emailCliente
        obtenerIdCliente = registro.CrearCliente()

        # Obtener el Id del cliente
        if obtenerIdCliente:
            for Id in obtenerIdCliente:
                session['IdCliente'] = Id[0] 

        # Registro del usuario
        idCliente = session['IdCliente']
        usuariosis = request.form['usuario']
        password = request.form['password']

        registro = RegistroCliente()
        registro.IdCliente = idCliente
        registro.UsuarioSis = usuariosis
        registro.Password = password
        registro.CrearUsuario()

        return redirect('/solicitarServicio')

    return render_template('admin/administrar_cliente.html')

@app.route('/solicitarServicio')
def ObtenerDatosCliente():

    dato = RegistroCliente()
    datos = dato.ObtenerClientes()
   
    return render_template('admin/solicitar_servicio.html', Datos = datos)

@app.route('/CrearSolicitud/<idCliente>')
def ObtenerDatosSolicitud(idCliente):
    
    servicio = Servicio()
    servicios = servicio.ObtenerServicio()
    
    entidad = TipoEntidad()
    entidades = entidad.ObtenerTipoEntidad()
    
    return render_template('admin/crear_solicitud.html', servicio = servicios, entidad = entidades)

@app.route('/CrearSolicitud/<idCliente>', methods=['GET', 'POST'])
def CrearSolicitud(idCliente):

    if request.method == 'POST':

        nombreEmpresa = request.form['nombreEmpresa']
        tipoEntidad = request.form['tipoEntidad']
        direccionEmpresa = request.form['direccionEmpresa']
        telefonoEmpresa = request.form['telefonoEmpresa']
        emailEmpresa = request.form['emailEmpresa']
        
        registro = RegistroClienteEntidad()
        registro.IdCliente = idCliente
        registro.NombreEmpresa = nombreEmpresa
        registro.TipoEntidad = tipoEntidad
        registro.DireccionEmpresa = direccionEmpresa
        registro.TelefonoEmpresa = telefonoEmpresa
        registro.EmailEmpresa = emailEmpresa
        obtenerIdClienteEntidad = registro.CrearClienteEntidad()

        # Obtener el Id de la entidad
        if obtenerIdClienteEntidad:
            for Id in obtenerIdClienteEntidad:
                session['IdEntidad'] = Id[0]

        # registrar la solicitud
        idClienteEntidad = session['IdEntidad']
        servicio = request.form['servicio']
        fechaSolicitud = request.form['fechaSolicitud']
        descripcion = request.form['descripcion']

        registro = Solicitud()
        registro.IdClienteEntidad = idClienteEntidad
        registro.Servicio = servicio
        registro.FechaSolicitud = fechaSolicitud
        registro.Descripcion = descripcion
        registro.CrearSolicitudAdmin()

        return redirect('/solicitarServicio')

    return render_template('admin/crear_solicitud.html')