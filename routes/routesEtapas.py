from flask import Flask, render_template, request, json, redirect, session, jsonify, make_response
from appconfig import app
from models.etapasAdmin import EtapasAdmin

@app.route('/verEtapas/<idProyecto>')
def verEtapas(idProyecto):

    if 'IdUsuario' in session and session['Rol'] == 2:

        etapas = EtapasAdmin()
        etapas.IdProyecto = idProyecto
        Etapas = etapas.ObtenerEtapasAdminxId()

        return render_template('admin/verEtapas.html', etapas = Etapas, idProyecto = idProyecto)
    else:
        return redirect('/')

@app.route('/verEtapas/', methods=['POST'])
def crearEtapas():

    if request.method == 'POST':

        idProyecto = request.form['idProyecto']
        nombre = request.form['NombreEtapa']
        responsable = request.form['Responsable']
        orden = request.form['Orden']
        fechaInicial = request.form['FechaInicial']
        fechaFinal = request.form['FechaFinal']

        documento = EtapasAdmin()
        documento.IdProyecto = idProyecto
        documento.Nombre = nombre
        documento.Responsable = responsable
        documento.Orden = orden
        documento.FechaInicial = fechaInicial
        documento.FechaFinal = fechaFinal
        documento.CrearEtapasAdmin()

        responseBody = { "xmsg": "" }
        return make_response(jsonify(responseBody), 200)

@app.route('/editarEtapa/', methods=['POST'])
def editarEtapa():

    if request.method == 'POST':

        idEtapa = request.form['idEtapa']
        nombreEtapa = request.form['NombreEtapa']
        responsable = request.form['Responsable']
        ordenEtapa = request.form['OrdenEtapa']
        fechaInicial = request.form['fechaInicial']
        fechaFinal = request.form['fechaFinal']

        documento = EtapasAdmin()
        documento.IdEtapa = idEtapa
        documento.Nombre = nombreEtapa
        documento.Responsable = responsable
        documento.Orden = ordenEtapa
        documento.FechaInicial = fechaInicial
        documento.FechaFinal = fechaFinal
        documento.EditarEtapaAdmin()

        responseBody = { "xmsg": "" }
        return make_response(jsonify(responseBody), 200)
    
@app.route('/deleteEtapa/', methods=['POST'])
def deleteEtapa():

    if request.method == 'POST':

        idEtapaDel = request.form['idEtapaDel']

        solicitud = EtapasAdmin()
        solicitud.IdEtapa = idEtapaDel
        solicitud.DeleteEtapaAdmin()

        responseBody = { "xmsg": "" }
        return make_response(jsonify(responseBody), 200)