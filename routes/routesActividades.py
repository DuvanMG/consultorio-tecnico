from flask import Flask, render_template, request, json, redirect, session, jsonify, make_response
from appconfig import app
from models.actividadesAdmin import ActividadesAdmin
from werkzeug.utils import secure_filename
import json, os

@app.route('/actividadDescargarArchivo/', methods=['POST'])
def actividadDescargarArchivo():

    if request.method == 'POST':

        idActividad = request.form["IdActividad"]

        Actividad = ActividadesAdmin()
        Actividad.IdActividad = idActividad
        ActividadArchivo = Actividad.ObtenerActividadArchivoAdminxId()

        if ActividadArchivo:
            for Id in ActividadArchivo:
                session['nombreArchivo'] = Id[4]

        nombreArchivo = session['nombreArchivo']

        return {"status": True, "xmsg":"", "nombreArchivo": nombreArchivo} 

@app.route('/actividades/', methods=['POST'])
def actividades():

    if request.method == 'POST':

        idEtapa = request.form["xmyRowId"]
        
        actividad = ActividadesAdmin()
        actividad.IdEtapa = idEtapa
        actividades = actividad.ObtenerActividadesAdminxId()

        return {"status": True, "xmsg":"", "listaactividades": actividades}

@app.route('/agregarActividad/', methods=['POST'])
def agregarActividad():

    if request.method == 'POST':

        idetapa = request.form['idetapa']
        nombre = request.form['Nombre']
        descripcion = request.form['Descripcion']

        actividad = ActividadesAdmin()
        actividad.IdEtapa = idetapa
        actividad.Nombre = nombre
        actividad.Descripcion = descripcion
        actividad.CrearActividadAdmin()

        responseBody = { "xmsg": "" }
        return make_response(jsonify(responseBody), 200)

@app.route('/agregarActividadDocumento/', methods=['POST'])
def agregarActividadDocumento():

    if request.method == 'POST':

        idActividad = request.form['IdActividadSubir']
        fileSave = request.files['file1']
        
        filename = secure_filename(fileSave.filename)
        fileSave.save(os.path.join('adjunto', filename))

        actividad = ActividadesAdmin()
        actividad.IdActividad = idActividad
        actividad.File = filename
        actividad.UpdateActividadAdmin()

        responseBody = { "xmsg": "" }
        return make_response(jsonify(responseBody), 200)

@app.route('/deleteActividad/', methods=['POST'])
def deleteActividad():

    if request.method == 'POST':

        idActividad = request.form['IdActividad']

        actividad = ActividadesAdmin()
        actividad.IdActividad = idActividad
        actividad.DeleteActividadAdmin()

        responseBody = { "xmsg": "" }
        return make_response(jsonify(responseBody), 200)