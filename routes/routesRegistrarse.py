from flask import Flask, render_template, request, json, redirect, session
from appconfig import app
from models.tipoDocumento import TipoDocumento
from models.tipoEntidad import TipoEntidad
from models.servicio import Servicio
from models.registrarse import RegistroCliente
from models.registroClienteEntidad import RegistroClienteEntidad
from models.solicitud import Solicitud

@app.route('/registrarse', methods=['GET'])
def ObtenerRegistro():

    documento = TipoDocumento()
    documentos = documento.ObtenerTipoDocumento()

    entidad = TipoEntidad()
    entidades = entidad.ObtenerTipoEntidad()

    servicio = Servicio()
    servicios = servicio.ObtenerServicio() 

    return render_template('/registrarse.html', entidad = entidades, documento = documentos, servicio = servicios)

@app.route('/registrarse', methods=['GET', 'POST'])
def CrearRegistro():

    if request.method == 'POST':

        # Registro del cliente
        nombreCliente = request.form['nombreCliente']
        apellidoCliente = request.form['apellidoCliente']
        tipodocumento = request.form['tipodocumento']
        numerodocumento = request.form['numerodocumento']
        direccionCliente = request.form['direccionCliente']
        celular = request.form['celular']
        emailCliente = request.form['emailCliente']        
        usuariosis = request.form['usuario']

        registro = RegistroCliente()
        registro.TipoDocumento = tipodocumento
        registro.numeroDocumento = numerodocumento
        registro.NombreCliente = nombreCliente
        registro.ApellidoCliente = apellidoCliente
        registro.Celular = celular
        registro.DireccionCliente = direccionCliente
        registro.EmailCliente = emailCliente
        registro.UsuarioSis = usuariosis
        obtenerIdCliente = registro.CrearCliente()

        # Obtener el Id del cliente
        if obtenerIdCliente:
            for Id in obtenerIdCliente:
                session['IdCliente'] = Id[0]

        # Registro del usuario
        idCliente = session['IdCliente']
        usuariosis = request.form['usuario']
        password = request.form['password']

        registro = RegistroCliente()
        registro.IdCliente = idCliente
        registro.UsuarioSis = usuariosis
        registro.Password = password
        registro.CrearUsuario()

        # registrar la entidad
        idCliente = session['IdCliente']
        nombreEmpresa = request.form['nombreEmpresa']
        tipoEntidad = request.form['tipoEntidad']
        direccionEmpresa = request.form['direccionEmpresa']
        telefonoEmpresa = request.form['telefonoEmpresa']
        emailEmpresa = request.form['emailEmpresa']

        registro = RegistroClienteEntidad()
        registro.IdCliente = idCliente
        registro.NombreEmpresa = nombreEmpresa
        registro.TipoEntidad = tipoEntidad
        registro.DireccionEmpresa = direccionEmpresa
        registro.TelefonoEmpresa = telefonoEmpresa
        registro.EmailEmpresa = emailEmpresa
        obtenerIdClienteEntidad = registro.CrearClienteEntidad()

        # Obtener el Id de la entidad
        if obtenerIdClienteEntidad:
            for Id in obtenerIdClienteEntidad:
                session['IdEntidad'] = Id[0]

        # registrar la solicitud
        idClienteEntidad = session['IdEntidad']
        servicio = request.form['servicio']
        fechaSolicitud = request.form['fechaSolicitud']
        descripcion = request.form['descripcion']

        registro = Solicitud()
        registro.IdClienteEntidad = idClienteEntidad
        registro.Servicio = servicio
        registro.FechaSolicitud = fechaSolicitud
        registro.Descripcion = descripcion
        registro.CrearSolicitud()

        return redirect('/login')

    return render_template('/registrarse.html')