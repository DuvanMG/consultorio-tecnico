from flask import Flask, render_template, request, json, redirect, session, jsonify, make_response
from appconfig import app
from models.tipoEntidad import TipoEntidad

@app.route('/tipoentidad', methods=['POST','GET'])
def tipoentidad():

    if request.method == 'GET':
        
        entidad = TipoEntidad()
        Entidad = entidad.ObtenerTipoEntidad()

        return render_template('admin/table_entidad.html', Entidades = Entidad)

    if request.method == 'POST':

        Nombre = request.form['Nombre']
        Descripcion = request.form['Descripcion']

        documento = TipoEntidad()
        documento.Nombre = Nombre
        documento.Descripcion = Descripcion
        documento.CrearTipoEntidad()

        return redirect('tipoentidad')

    return render_template('admin/table_entidad.html')

@app.route('/editEntidad', methods=['POST'])
def editEntidad():

    if request.method == 'POST':

        idEntidad = request.form['idEntidad']
        Nombre = request.form['Nombre']
        Descripcion = request.form['Descripcion']

        documento = TipoEntidad()
        documento.IdEntidad = idEntidad
        documento.Nombre = Nombre
        documento.Descripcion = Descripcion
        documento.EditarEntidad()

        responseBody = { "xmsg": "" }
        return make_response(jsonify(responseBody), 200)
    
@app.route('/deleteEntidad', methods=['POST'])
def deleteEntidad():

    if request.method == 'POST':

        idEntidadDel = request.form['idEntidadDel']

        solicitud = TipoEntidad()
        solicitud.IdEntidad = idEntidadDel
        solicitud.DeleteEntidad()

        responseBody = { "xmsg": "" }
        return make_response(jsonify(responseBody), 200)