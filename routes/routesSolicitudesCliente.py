from flask import Flask, render_template, request, json, redirect, session, make_response, jsonify
from appconfig import app
from models.gestionClientes import GestionClientes
      
@app.route('/solicitudesCliente', methods=['GET'])
def solicitudesCliente():

   if 'IdUsuario' in session and session['Rol'] == 1:
        
      Usuario = session['UsuarioSis']
      IdCliente = session['IdCliente']

      usuario = Usuario.title()
      solicitudes = GestionClientes()
      solicitudes.IdCliente = IdCliente
      Solicitudes = solicitudes.ObtenerSolicitudesxId()
      
      return render_template('cliente/solicitudesCliente.html', usuario = Usuario.title(), Solicitudes = Solicitudes) 

   else:
      return redirect('/administrarClientes')

@app.route('/solicitudesCliente', methods=['POST'])
def updateSolicitudesCliente():

   if request.method == 'POST':

      idSolicitud = request.form['IdSolicitud']
      descripcion = request.form['Descripcion']
      
      solicitud = GestionClientes()
      solicitud.IdSolicitud = idSolicitud
      solicitud.Descripcion = descripcion
      solicitud.apdateSolicitud()

      responseBody = { "xmsg": "" }
      return make_response(jsonify(responseBody), 200)

@app.route('/deleteSolicitudesCliente', methods=['POST'])
def deleteSolicitudesCliente():

   if request.method == 'POST':

      idSolicitud = request.form['IdSolicitud']
      print(idSolicitud)
      solicitud = GestionClientes()
      solicitud.IdSolicitud = idSolicitud
      solicitud.deleteSolicitud()

      responseBody = { "xmsg": "" }
      return make_response(jsonify(responseBody), 200)