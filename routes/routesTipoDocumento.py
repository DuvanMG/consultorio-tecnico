from flask import Flask, render_template, request, json, redirect, session, jsonify, make_response
from appconfig import app
from models.tipoDocumento import TipoDocumento

@app.route('/tipodocumento', methods=['POST','GET'])
def tipodocumento():

    if request.method == 'GET': 

        documento = TipoDocumento()
        Documento = documento.ObtenerTipoDocumento()

        return render_template('admin/table_documento.html', Documentos = Documento)

    if request.method == 'POST':

        Nombre = request.form['Nombre']
        Descripcion = request.form['Descripcion']

        documento = TipoDocumento()
        documento.Nombre = Nombre
        documento.Descripcion = Descripcion
        documento.CrearTipoDocumento()

        return redirect('/tipodocumento')

    return render_template('admin/table_documento.html')

@app.route('/editDocumento', methods=['POST'])
def editDocumento():

    if request.method == 'POST':

        idDocumento = request.form['idDocumento']
        Nombre = request.form['Nombre']
        Descripcion = request.form['Descripcion']

        documento = TipoDocumento()
        documento.IdDocumento = idDocumento
        documento.Nombre = Nombre
        documento.Descripcion = Descripcion
        documento.EditarDocumento()

        responseBody = { "xmsg": "" }
        return make_response(jsonify(responseBody), 200)
    
    return render_template('admin/table_documento.html')

@app.route('/deleteDocumento', methods=['POST'])
def deleteDocumento():

    if request.method == 'POST':

        idDocumentoDel = request.form['idDocumentoDel']

        solicitud = TipoDocumento()
        solicitud.IdDocumento = idDocumentoDel
        solicitud.DeleteDocumento()

        responseBody = { "xmsg": "" }
        return make_response(jsonify(responseBody), 200)
