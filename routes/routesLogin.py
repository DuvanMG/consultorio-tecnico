from flask import Flask, render_template, request, json, redirect, session
from appconfig import app
from models.login import Login

@app.route('/login', methods=['POST', 'GET'])
def login():

    if request.method == 'POST':

        Usuariosis = request.form['usuariosis']
        password = request.form['password']

        ingresar = Login()
        ingresar.UsuarioSis = Usuariosis
        ingresar.Password = password

        registros = ingresar.ObtenerDatos()

        if registros:
            
            for registro in registros:

                session['IdUsuario'] = registro[0]
                session['Rol'] = registro[1]
                session['IdCliente'] = registro[2]
                session['UsuarioSis'] = registro[3]

                return redirect('/solicitudesCliente')
        else:       
            return redirect('/login')

    return render_template('login.html')