from flask import Flask, render_template, request, json, redirect, session
from appconfig import app

@app.route('/')
def inicioGeneral():
    
    return render_template('index.html')

@app.route('/logout')
def Logout():

    session.clear()
    return redirect('/')