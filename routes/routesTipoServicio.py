from flask import Flask, render_template, request, json, redirect, session, jsonify, make_response
from appconfig import app
from models.servicio import Servicio

@app.route('/servicios', methods=['POST','GET'])
def servicios():

    if request.method == 'GET':
        
        servicios = Servicio()
        Servicios = servicios.ObtenerServicio()

        return render_template('admin/table_servicios.html', Servicios = Servicios)

    if request.method == 'POST':

        Nombre = request.form['Nombre']
        Descripcion = request.form['Descripcion']

        documento = Servicio()
        documento.Nombre = Nombre
        documento.Descripcion = Descripcion
        documento.CrearTipoServicio()

        return redirect('servicios')

    return render_template('admin/table_servicios.html')

@app.route('/editServicio', methods=['POST'])
def editServicio():

    if request.method == 'POST':

        idServicio = request.form['idServicio']
        Nombre = request.form['Nombre']
        Descripcion = request.form['Descripcion']

        documento = Servicio()
        documento.IdServicio = idServicio
        documento.Nombre = Nombre
        documento.Descripcion = Descripcion
        documento.EditarServicio()

        responseBody = { "xmsg": "" }
        return make_response(jsonify(responseBody), 200)
    
@app.route('/deleteServicio', methods=['POST'])
def deleteServicio():

    if request.method == 'POST':

        idServicioDel = request.form['idServicioDel']

        solicitud = Servicio()
        solicitud.IdServicio = idServicioDel
        solicitud.DeleteServicio()

        responseBody = { "xmsg": "" }
        return make_response(jsonify(responseBody), 200)