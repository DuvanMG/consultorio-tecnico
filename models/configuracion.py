from dbconexion import cursor, conn

class DatosPersonales:

    IdCliente = 0
    Celular = ''
    Direccion = ''
    Email = ''

    def ObtenerDatosPersonales(self):
        
        sql_agregar_DatosPersonales = "CALL usp_consultarDatosPersonales(?,?)"
        params = (self.IdCliente,self.IdCliente)

        cursor.execute(sql_agregar_DatosPersonales,params)
        DatosPersonales = cursor.fetchall()

        return DatosPersonales

    def UpdateDatosPersonales(self):
      
        sql_agregar_UpdateDatosPersonales = "CALL usp_actualizarDatosPersonales(?,?,?,?)"
        params = (self.IdCliente, self.Celular, self.Direccion, self.Email)
       
        cursor.execute(sql_agregar_UpdateDatosPersonales,params)
        conn.commit()