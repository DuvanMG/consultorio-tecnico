from dbconexion import conn, cursor

class Proyectos:

    IdProyecto = 0
    IdServicio = 0
    IdSolicitud = 1
    Responsable = '' 
    FechaFinal = ''
    FechaInicial = ''
    Descripcion = ''
    Finalizado = 1

    def finalizarProyectoAdmin(self):

        sql_FinalizarProyectoAdmin = "CALL usp_actualizarProyectoFinalizar(?,?)"
        params = (self.IdProyecto, self.Finalizado)

        cursor.execute(sql_FinalizarProyectoAdmin,params)
        conn.commit()

    def ObtenerProyectos(self):

        cursor.execute("CALL usp_consultarProyectos")
        proyecto = cursor.fetchall()
        
        return proyecto

    def CrearProyectoSolicitud(self):

        sql_CrearProyecto = "CALL usp_insertarProyectoSolicitud(?,?,?,?,?,?)"
        params = (self.IdSolicitud, self.IdServicio, self.Descripcion, self.Responsable, self.FechaInicial, self.FechaFinal)

        cursor.execute(sql_CrearProyecto,params)
        conn.commit()