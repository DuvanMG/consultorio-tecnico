from dbconexion import cursor, conn

class Estados:

    IdEstado = 0
    Nombre = ""
    Descripcion = ""

    def ObtenerEstado(self):
      
        cursor.execute("SELECT * FROM estado")
        servicios = cursor.fetchall()
        
        return servicios

    def CrearEstado(self):
      
        sql_agregar_Estado = "CALL usp_insertarEstado(?,?)"
        params = (self.Nombre, self.Descripcion)
       
        cursor.execute(sql_agregar_Estado,params)
        conn.commit()

    def EditarEstado(self):
      
        sql_agregar_EditarEstado = "CALL usp_actualizarEstado(?,?,?)"
        params = (self.IdEstado, self.Nombre, self.Descripcion)
       
        cursor.execute(sql_agregar_EditarEstado,params)
        conn.commit()

    def DeleteEstado(self):
      
        sql_agregar_DeleteEstado = "CALL usp_deleteEstado(?,?)"
        params = (self.IdEstado, self.IdEstado)
       
        cursor.execute(sql_agregar_DeleteEstado,params)
        conn.commit() 