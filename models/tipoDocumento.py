from dbconexion import cursor, conn

class TipoDocumento:

    IdDocumento = 0
    Nombre = ''
    Descripcion = ''

    def ObtenerTipoDocumento(self):
      
        cursor.execute("SELECT * FROM tipo_documento ORDER BY Id_tipo_documento")
        documentos = cursor.fetchall()
        
        return documentos

    def CrearTipoDocumento(self):
      
        sql_agregar_TipoDocumento = "CALL usp_insertarTipo_documento(?,?)"
        params = (self.Nombre, self.Descripcion)
       
        cursor.execute(sql_agregar_TipoDocumento,params)
        conn.commit()

    def EditarDocumento(self):
      
        sql_agregar_EditarDocumento = "CALL usp_actualizarDocumento(?,?,?)"
        params = (self.IdDocumento, self.Nombre, self.Descripcion)
       
        cursor.execute(sql_agregar_EditarDocumento,params)
        conn.commit()

    def DeleteDocumento(self):
      
        sql_agregar_DeleteDocumento = "CALL usp_deleteDocumento(?,?)"
        params = (self.IdDocumento, self.IdDocumento)
       
        cursor.execute(sql_agregar_DeleteDocumento,params)
        conn.commit() 