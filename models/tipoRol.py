from dbconexion import cursor, conn

class TipoRol:

    IdRol = 0
    Nombre = ''
    Descripcion = ''
    
    def ObtenerRoles(self):

        cursor.execute("SELECT * FROM rol")
        Roles = cursor.fetchall()

        return Roles

    def CrearRol(self):
      
        sql_agregar_TipoRol = "CALL usp_insertarRol(?,?)"
        params = (self.Nombre, self.Descripcion)
       
        cursor.execute(sql_agregar_TipoRol,params)
        conn.commit()
    
    def EditarRol(self):
      
        sql_agregar_EditarRol = "CALL usp_actualizarRol(?,?,?)"
        params = (self.IdRol, self.Nombre, self.Descripcion)
       
        cursor.execute(sql_agregar_EditarRol,params)
        conn.commit()

    def DeleteRol(self):
      
        sql_agregar_DeleteEntidad = "CALL usp_deleteRol(?,?)"
        params = (self.IdRol, self.IdRol)
       
        cursor.execute(sql_agregar_DeleteEntidad,params)
        conn.commit()