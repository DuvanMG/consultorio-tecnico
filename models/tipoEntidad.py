from dbconexion import cursor, conn

class TipoEntidad:

    IdEntidad = 0
    Nombre = ''
    Descripcion = ''
    
    def ObtenerTipoEntidad(self):
      
        cursor.execute("SELECT * FROM tipo_entidad ORDER BY Id_tipo_entidad")
        entidad = cursor.fetchall()
        return entidad

    def CrearTipoEntidad(self):
      
        sql_agregar_TipoEntidad = "CALL usp_insertarTipo_entidad(?,?)"
        params = (self.Nombre, self.Descripcion)
       
        cursor.execute(sql_agregar_TipoEntidad,params)
        conn.commit()

    def EditarEntidad(self):
      
        sql_agregar_EditarEntidad = "CALL usp_actualizarEntidad(?,?,?)"
        params = (self.IdEntidad, self.Nombre, self.Descripcion)
       
        cursor.execute(sql_agregar_EditarEntidad,params)
        conn.commit()

    def DeleteEntidad(self):
      
        sql_agregar_DeleteEntidad = "CALL usp_deleteEntidad(?,?)"
        params = (self.IdEntidad, self.IdEntidad)
       
        cursor.execute(sql_agregar_DeleteEntidad,params)
        conn.commit()