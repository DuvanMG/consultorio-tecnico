from dbconexion import conn, cursor

class Solicitud:

    IdClienteEntidad = 0
    Servicio = 0
    Estado = 1
    FechaSolicitud = ''
    Descripcion = ''

    def ObtenerSolicitudes(self):
        
        cursor.execute("CALL usp_consultarSolicitud_servicio")
        Solicitudes = cursor.fetchall()
        
        return Solicitudes
        
    def CrearSolicitud(self):

        sql_agregar_Solicitud = "CALL usp_insertarSolicitudServicio(?,?,?,?,?)"
        params = (self.IdClienteEntidad, self.Servicio, self.Estado, self.FechaSolicitud, self.Descripcion)
       
        cursor.execute(sql_agregar_Solicitud,params)
        conn.commit() 

    def CrearSolicitudAdmin(self):

        sql_agregar_Solicitud = "CALL usp_insertarSolicitudServicio(?,?,?,?,?)"
        params = (self.IdClienteEntidad[0], self.Servicio, self.Estado, self.FechaSolicitud, self.Descripcion)
       
        cursor.execute(sql_agregar_Solicitud,params)
        conn.commit() 