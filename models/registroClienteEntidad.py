from dbconexion import conn, cursor 

class RegistroClienteEntidad:

    IdCliente = 1
    TipoEntidad = 0
    NombreEmpresa = ''
    DireccionEmpresa = ''
    TelefonoEmpresa = ''
    EmailEmpresa = ''

    def CrearClienteEntidad(self):
        
        sql_agregar_clienteEntidad = "CALL usp_insertarCliente_entidad(?,?,?,?,?,?)"
        params = (self.IdCliente, self.TipoEntidad, self.NombreEmpresa, self.DireccionEmpresa, self.TelefonoEmpresa, self.EmailEmpresa)

        cursor.execute(sql_agregar_clienteEntidad,params)
        registro = cursor.fetchall()

        return registro 
        
    def CrearClienteEntidadLogeado(self):
        
        cursor.execute("set @myparam=0;")
        
        sql_agregar_clienteEntidad = "CALL usp_insertarCliente_entidadLogeado(?,?,?,?,?,?,@myparam)"
        params = (self.IdCliente, self.TipoEntidad, self.NombreEmpresa, self.DireccionEmpresa, self.TelefonoEmpresa, self.EmailEmpresa)
        
        cursor.execute(sql_agregar_clienteEntidad,params)
        conn.commit()

        cursor.execute("SELECT @xlastInsert")
        registro = cursor.fetchall()

        return registro 
                
