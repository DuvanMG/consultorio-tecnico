from dbconexion import cursor, conn

class ActividadesAdmin:

    IdEtapa = 0
    SinTerminar = 0
    IdActividad = 0
    Terminada = 1
    Nombre = ''
    Descripcion = ''
    File = ''

    def CrearActividadAdmin(self):
        
        sql_agregar_CrearActividadAdmin = "CALL usp_insertarActividad(?,?,?,?)"
        params = (self.IdEtapa, self.Nombre, self.Descripcion, self.SinTerminar)

        cursor.execute(sql_agregar_CrearActividadAdmin,params)
        conn.commit()

    def ObtenerActividadArchivoAdminxId(self):

        cursor.execute("set @myparam=0;")

        sql_agregar_ObtenerActividadArchivoAdminxId = "CALL usp_consultarActividadArchivoAdminxId(?,?,@myparam)"
        params = (self.IdActividad,0)

        cursor.execute(sql_agregar_ObtenerActividadArchivoAdminxId,params)
        actividad = cursor.fetchall()

        return actividad
    
    def ObtenerActividadesAdminxId(self):

        cursor.execute("set @myparam=0;")

        sql_agregar_ActividadesAdminxId = "CALL usp_consultarActividadesAdminxId(?,?,@myparam)"
        params = (self.IdEtapa,0)

        cursor.execute(sql_agregar_ActividadesAdminxId,params)
        actividades = cursor.fetchall()

        return actividades

    def DeleteActividadAdmin(self):
        
        sql_agregar_DeleteActividadAdmin = "CALL usp_DeleteActividad(?,?)"
        params = (self.IdActividad,self.IdActividad)

        cursor.execute(sql_agregar_DeleteActividadAdmin,params)
        conn.commit()

    def UpdateActividadAdmin(self):
        
        sql_agregar_UpdateActividadAdmin = "CALL usp_actualizarActividad(?,?,?)"
        params = (self.IdActividad,self.File,self.Terminada)

        cursor.execute(sql_agregar_UpdateActividadAdmin,params)
        conn.commit()