from dbconexion import cursor, conn

class EtapasAdmin:

    IdEtapa = 0
    IdProyecto = 0
    Orden = ''
    Nombre = ''
    Responsable = ''
    FechaInicial = ''
    FechaFinal = ''

    def ObtenerEtapasAdminxId(self):
        
        cursor.execute("set @myparam=0;")

        sql_agregar_EtapasAdminxId = "CALL usp_consultarEtapasAdminxId(?,?,@myparam)"
        params = (self.IdProyecto,self.IdProyecto)

        cursor.execute(sql_agregar_EtapasAdminxId,params)
        solicitudes = cursor.fetchall()

        return solicitudes

    def CrearEtapasAdmin(self):
        
        sql_agregar_CrearEtapasAdmin = "CALL usp_insertarEtapa(?,?,?,?,?,?)"
        params = (self.IdProyecto,self.Orden,self.Nombre,self.Responsable,self.FechaInicial,self.FechaFinal)

        cursor.execute(sql_agregar_CrearEtapasAdmin,params)
        conn.commit()

    def EditarEtapaAdmin(self):
        
        sql_agregar_CrearEtapasAdmin = "CALL usp_actualizarEtapa(?,?,?,?,?,?)"
        params = (self.IdEtapa,self.Orden,self.Nombre,self.Responsable,self.FechaInicial,self.FechaFinal)

        cursor.execute(sql_agregar_CrearEtapasAdmin,params)
        conn.commit()

    def DeleteEtapaAdmin(self):
        
        sql_agregar_CrearEtapasAdmin = "CALL usp_deleteEtapa(?,?)"
        params = (self.IdEtapa,self.IdEtapa)

        cursor.execute(sql_agregar_CrearEtapasAdmin,params)
        conn.commit()