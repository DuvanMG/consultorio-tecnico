from dbconexion import cursor, conn

class Mensajeria:

    IdCliente = 0
    IdProyecto = 0
    Mensaje = ''
    Respuesta = ''
    FechaMensaje = ''
    FechaRespuesta = ''

    def CrearMensajeria(self):
        
        sql_agregar_CrearMensajeria = "CALL usp_insertarMensajeriaCliente(?,?,?,?)"
        params = (self.IdCliente,self.IdProyecto,self.Mensaje,self.FechaMensaje)

        cursor.execute(sql_agregar_CrearMensajeria,params)
        conn.commit()

    def ObtenerMensajeriaxId(self):

        sql_agregar_MensajeriaxId = "CALL usp_consultarMensajeriaxId(?,?)"
        params = (self.IdProyecto,self.IdProyecto)

        cursor.execute(sql_agregar_MensajeriaxId,params)
        messages = cursor.fetchall()

        return messages

    def ObtenerMessagesAdmin(self):
        
        cursor.execute("set @myparam=0;")

        sql_agregar_MessagesAdmin = "CALL usp_consultarMessagesAdmin(?,?,@myparam)"
        params = (self.IdCliente,self.IdCliente)
        
        cursor.execute(sql_agregar_MessagesAdmin,params)
        messages = cursor.fetchall()
        
        return messages

    def CrearMessagesAdmin(self):
        
        sql_agregar_CrearMessagesAdmin = "CALL usp_insertarMessagesAdmin(?,?,?,?)"
        params = (self.IdProyecto,self.IdCliente,self.Respuesta,self.FechaRespuesta)

        cursor.execute(sql_agregar_CrearMessagesAdmin,params)
        conn.commit()