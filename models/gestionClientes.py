from dbconexion import cursor, conn

class GestionClientes:

    IdCliente = 0
    IdSolicitud = 0
    Descripcion = ''

    def ObtenerSolicitudesxId(self):
        
        sql_agregar_SolicitudesxId = "CALL usp_consultarSolicitud_servicioxID(?,?)"
        params = (self.IdCliente,self.IdCliente)

        cursor.execute(sql_agregar_SolicitudesxId,params)
        solicitudes = cursor.fetchall()

        return solicitudes

    def ObtenerProyectosxId(self):
        
        sql_agregar_ProyectosxId = "CALL usp_consultarProyectosxID(?,?)"
        params = (self.IdCliente,self.IdCliente)

        cursor.execute(sql_agregar_ProyectosxId,params)
        Proyectos = cursor.fetchall()

        return Proyectos

    def apdateSolicitud(self):
        
        sql_agregar_apdateSolicitud = "CALL usp_actualizarSolicitudServicio(?,?)"
        params = (self.IdSolicitud,self.Descripcion)

        cursor.execute(sql_agregar_apdateSolicitud,params)
        conn.commit()

    def deleteSolicitud(self):
        
        sql_agregar_apdateSolicitud = "CALL usp_deleteSolicitudServicio(?,?)"
        params = (self.IdSolicitud,self.IdSolicitud)

        cursor.execute(sql_agregar_apdateSolicitud,params)
        conn.commit()