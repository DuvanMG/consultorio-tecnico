from dbconexion import conn, cursor 

class RegistroCliente:

    IdCliente = 0
    TipoDocumento = 0
    numeroDocumento = 0
    Rol = 1
    NombreCliente = ''
    ApellidoCliente = ''
    Celular = ''
    DireccionCliente = ''
    EmailCliente = ''
    UsuarioSis = ''
    Password = ''

    def CrearCliente(self):

        sql_agregar_cliente = "CALL usp_insertarCliente(?,?,?,?,?,?,?)"
        params = (self.TipoDocumento, self.numeroDocumento, self.NombreCliente, self.ApellidoCliente, self.Celular, self.DireccionCliente, self.EmailCliente)

        cursor.execute(sql_agregar_cliente,params)
        registro = cursor.fetchall()

        return registro
    
    def CrearUsuario(self):

        sql_agregar_usuario = "CALL usp_insertarUsuario(?,?,?,?)"
        params = (self.Rol, self.IdCliente, self.UsuarioSis, self.Password)

        cursor.execute(sql_agregar_usuario,params)
        conn.commit()

    def ObtenerClientes(self):

        cursor.execute("CALL usp_consultarCliente")
        datos = cursor.fetchall()
        
        return datos

