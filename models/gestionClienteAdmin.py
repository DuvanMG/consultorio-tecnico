from dbconexion import cursor, conn

class GestionClienteAdmin:

    IdCliente = 0
    DireccionCliente = ''
    Celular = ''
    EmailCliente = ''

    def updateClienteAdmin(self):
        
        sql_agregar_updateClienteAdmin = "CALL usp_actualizarClienteAdmin(?,?,?,?)"
        params = (self.IdCliente,self.Celular,self.DireccionCliente,self.EmailCliente)

        cursor.execute(sql_agregar_updateClienteAdmin,params)
        conn.commit()