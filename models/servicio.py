from dbconexion import cursor, conn

class Servicio:

    IdServicio = 0
    Nombre = ''
    Descripcion = ''

    def ObtenerServicio(self):
      
        cursor.execute("SELECT * FROM servicio ORDER BY Id_servicio")
        servicios = cursor.fetchall()
        
        return servicios

    def CrearTipoServicio(self):
      
        sql_agregar_TipoServicio = "CALL usp_insertarTipo_Servicio(?,?)"
        params = (self.Nombre, self.Descripcion)
       
        cursor.execute(sql_agregar_TipoServicio,params)
        conn.commit()

    def EditarServicio(self):
      
        sql_agregar_EditarServicio = "CALL usp_actualizarServicio(?,?,?)"
        params = (self.IdServicio, self.Nombre, self.Descripcion)
       
        cursor.execute(sql_agregar_EditarServicio,params)
        conn.commit()

    def DeleteServicio(self):
      
        sql_agregar_DeleteEntidad = "CALL usp_deleteServicio(?,?)"
        params = (self.IdServicio, self.IdServicio)
       
        cursor.execute(sql_agregar_DeleteEntidad,params)
        conn.commit()