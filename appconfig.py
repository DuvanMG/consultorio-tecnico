from flask import Flask, render_template, request, redirect, session

app = Flask(__name__)

from routes import routesLogin
from routes import routesRegistrarse
from routes import routesInicio
from routes import routesSolicitudCliente
from routes import routesGestionClientes
from routes import routesProyectos
from routes import routesGestionProyectos
from routes import routesSolicitudesCliente
from routes import routesEtapas
from routes import routesMessages
from routes import routesActividades
from routes import configuracion
from routes import routesTipoDocumento
from routes import routesTipoEntidad
from routes import routesTipoServicio
from routes import routesTipoRol
from routes import routesEstado

app.secret_key = 'sistemaobrascivilesapp'
